/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.03.05
 */

package controllers

import javax.inject.{Inject, Singleton}

import com.github.nscala_time.time.Imports._
import play.api.mvc.Results
import play.api.libs.json._
import play.api.libs.functional.syntax._
import cats._
import cats.data.EitherT
import cats.implicits._
import controllers.templates.PlayTemplates
import models.entities._
import models.dao.{NotationsDAO, PicturesDAO, PostsDAO, TextDataDAO}
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

@Singleton
class PostController @Inject() (
  val controllerProvider: GeneralControllerProvider,
  val postsDao: PostsDAO,
  val picturesDao: PicturesDAO,
  val textsDao: TextDataDAO,
  val notationsDao: NotationsDAO,
  val postWrapperUtils: PostWrapperUtils
) extends GeneralController with DataRequestErrors {

  def currentUserPosts = AuthorizedAction.async { req =>
    val AuthorizedRequest(user, nots, request) = req
    for {
      posts <- postsDao.getAllOf(user)
    } yield {
      val page = PlayTemplates.main("User posts")(Some(user)) {
        PlayTemplates.posts.review_posts(posts, true)
      }
      Ok(page)
    }
  }

  def userPosts(userId: Int) = UserAction.async { req =>
    val UserRequest(optUser, nots, request) = req

    def fetchUser(id: Int) = EitherT[Future, Error, User] {
      usersDao.getBy(id).map { optUser =>
        Either.fromOption(optUser, Error.NotFound("User with this id doesn't exist"))
      }
    }

    val operation: EitherT[Future, Error, (Seq[Post], Boolean)] = for {
      requiredUser <- fetchUser(userId)
      isOwner = optUser.map(_ == requiredUser).getOrElse(false)
      posts <- EitherT.right(postsDao.getAllOf(requiredUser))
    } yield (posts, isOwner)

    operation.fold(
      _ match {
        case Error.NotFound(msg) => Redirect(routes.Application.index).flashing("error" -> msg)
        case Error.NotFound => Results.NotFound
        case _ => Results.InternalServerError
      },
      tuple => {
        val (posts, owner) = tuple
        val page = PlayTemplates.main("User posts")(optUser) {
          PlayTemplates.posts.review_posts(posts, owner)
        }
        Ok(page)
      }
    )
  }

  def all = UserAction.async { req =>
    val UserRequest(optUser, nots, _) = req
    for {
      posts <- postsDao.getAll
      postWrappers <- Future.sequence { posts.map { postWrapperUtils.build(_) } }
      page = PlayTemplates.main("Tolkotvor posts")(optUser) {
        PlayTemplates.posts.posts(postWrappers, staticRoute)
      }
    } yield Ok(page)
  }

  def newPost() = AuthorizedAction.async { req =>
    val AuthorizedRequest(user, nots, request) = req
    val time = DateTime.now
    val post = Post(None, user.id.get, "Your post name", time, time)
    for {
      addedPost <- postsDao.add(post)
    } yield Redirect(routes.PostController.edit(addedPost.id.get))
  }

  object PostUtils {
    case class Fragment(`type`: String, id: Int, order: Int)
    case class UpdatedPost(id: Int, title: String, fragments: Seq[Fragment])
    implicit val fragmentReads: Reads[Fragment] = (
      (__ \ "type").read[String] and
      (__ \ "fragmentId").read[Int] and
      (__ \ "order").read[Int]
    )(Fragment.apply _)
    implicit val postReads: Reads[UpdatedPost] = (
      (__ \ "postId").read[Int] and
        (__ \ "postTitle").read[String] and
        (__ \ "fragments").read[Seq[Fragment]]
    )(UpdatedPost.apply _)

    def postBelongsToRequester(post: Post, user: User): EitherT[Future, Error, Unit] = {
      val errOrOk = (post.userId == user.id.get) match {
        case true => Right(())
        case _ => Left(Error.PermissionDenied)
      }
      EitherT.fromEither[Future](errOrOk)
    }
  }

  def delete(id: Int) = AuthorizedAction.async { implicit req =>
    val AuthorizedRequest(user, nots, request) = req

    val operationResult: EitherT[Future, Error, Unit] = for {
      existedPost <- EitherT( postsDao.getBy(id).map { Either.fromOption(_, Error.NotFound) })
      _ <- PostUtils.postBelongsToRequester(existedPost, user)
      _ <- EitherT[Future, Nothing, Unit](postWrapperUtils.delete(existedPost).map{ Right(_) })
      isDeleted <- EitherT(postsDao.delete(existedPost).map{ Either.cond[Error, Unit](_, (), Error.UnknownError) })
    } yield isDeleted

    operationResult.fold(
      _ match {
        case Error.NotFound => Results.NotFound
        case Error.PermissionDenied => Results.Forbidden
        case Error.BadRequest => Results.BadRequest
        case _ => Results.InternalServerError
      },
      success => Results.NoContent
    )
  }

  def update(id: Int) = AuthorizedAction.async(parse.json) { req =>
    import PostUtils._

    val AuthorizedRequest(user, nots, request) = req
    val errOrPost = req.body.validate[UpdatedPost].asEither.leftMap(_ => Error.BadRequest)

    def getPost(id: Int) = EitherT[Future, Error.NotFound.type, Post] {
      postsDao
        .getBy(id)
        .map { Either.fromOption(_, Error.NotFound) }
    }


    def updatePost(post: Post) = EitherT[Future, Error, Unit] {
      postsDao.update(post).map {
        case true => Right(())
        case false => Left(Error.UnknownError)
      }
    }

    def processFragmentData(postId: Int, fragments: Seq[Fragment]): Either[Error, Seq[PostContent]] = {
      val validTypes = Seq("pictures", "texts", "ptexts")

      def checkTypes(fragments: Seq[Fragment]): Either[Error.BadRequest, Unit] = fragments.forall { f => validTypes.contains(f.`type`) } match {
        case true => Right(())
        case _ => Left(Error.BadRequest("Invalid fragment type"))
      }

      def mapFragments(fragments: Seq[Fragment]): Seq[PostContent] = {
        fragments.map { _ match {
          case Fragment("pictures", id, order) => PostContent(postId, Some(id), None, None, Some(order))
          case Fragment("texts", id, order) => PostContent(postId, None, Some(id), None, Some(order))
          case Fragment("ptexts", id, order) => PostContent(postId, None, None, Some(id), Some(order))
        }}
      }
      for {
        _ <- checkTypes(fragments)
        postContents <- Right(mapFragments(fragments))
      } yield postContents
      
    }

    def updatePostContent(contents: Seq[PostContent]) = EitherT[Future, Error, Unit] {
      postsDao.updateOrder(contents).map {
        case true => Right(())
        case _ => Left(Error.UnknownError)
      }
    }

    val operationResult: EitherT[Future, Error, Post] = for {
      postData <- EitherT.fromEither[Future](errOrPost)
      UpdatedPost(_, title, fragments) = postData
      existedPost <- getPost(id)
      _ <- postBelongsToRequester(existedPost, user)
      updatedPost = existedPost.copy(title = title, updated = DateTime.now)
      _ <- updatePost(updatedPost)
      postContents <- EitherT.fromEither[Future](processFragmentData(id, fragments: Seq[Fragment]))
      _ <- updatePostContent(postContents)
    } yield updatedPost

    operationResult.fold(
      _ match {
        case Error.NotFound => Results.NotFound
        case Error.PermissionDenied => Results.Forbidden
        case Error.BadRequest => Results.BadRequest
        case _ => Results.InternalServerError
      },
      post => Ok
    )
  }

  def review(id: Int) = UserAction.async { implicit req =>
    val UserRequest(optUser, nots, _) = req
    val operationResult: EitherT[Future, Error, PostWrapper] = for {
      post <- EitherT {
        postsDao.getBy(id).map{ Either.fromOption(_, Error.NotFound("Post with this id doesn't exist!"))}
      }
      postWrapper <- EitherT.right(postWrapperUtils.build(post))
    } yield postWrapper
    operationResult.fold(
      _ match {
        case Error.NotFound(msg) => Redirect(routes.Application.index).flashing("error" -> msg)
        case Error.NotFound => Results.NotFound
        case _ => Results.InternalServerError
      },
      postWrapper => {
        val page = PlayTemplates.main(s"Post '${postWrapper.post.title}'")(optUser, nots)(
          PlayTemplates.posts.post_review(staticRoute, postWrapper, textNotationForm = TextNotation.form)
        )
        Ok(page)
      }
    )
  }

  

  def edit(id: Int) = AuthorizedAction.async { implicit req =>
    val AuthorizedRequest(user, nots, request) = req
    val picForm = utils.PictureUtils.uploadPictureForm
    val textForm = utils.TextDataUtils.publicationForm
    val textNotationForm = TextNotation.form

    def postBelongsToRequester(post: Post, user: User): EitherT[Future, Error, Unit] = {
      val errOrOk = (post.userId == user.id.get) match {
        case true => Right(())
        case _ => Left(Error.PermissionDenied)
      }
      EitherT.fromEither[Future](errOrOk)
    }

    val operationResult: EitherT[Future, Error, PostWrapper] = for {
      post <- EitherT {
        postsDao.getBy(id).map{ Either.fromOption(_, Error.NotFound("Post with this id doesn't exist!"))}
      }
      _ <- postBelongsToRequester(post, user)
      postWrapper <- EitherT.right(postWrapperUtils.build(post))
    } yield postWrapper
    operationResult.fold(
      _ match {
        case Error.NotFound(msg) => Redirect(routes.Application.index).flashing("error" -> msg)
        case Error.NotFound => Results.NotFound
        case Error.PermissionDenied(msg) => Redirect(routes.Application.index).flashing("error" -> msg)
        case _ => Results.InternalServerError
      },
      postWrapper => {
        val page = PlayTemplates.main("Editing")(Some(user), nots)(
          PlayTemplates.posts.edit_post(picForm, textForm)(staticRoute, postWrapper.post, postWrapper.fragments, textNotationForm)
        )
        Ok(page)
      }
    )
  }
}
