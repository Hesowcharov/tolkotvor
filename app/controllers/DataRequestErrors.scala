/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.03.04
 */

package controllers

trait DataRequestErrors {
  sealed trait Error
  object Error {
    final case object BadRequest extends Error
    final case class BadRequest(error: String) extends Error
    final case object NotFound extends Error
    final case class NotFound(error: String) extends Error
    final case object PermissionDenied extends Error
    final case class PermissionDenied(error: String) extends Error
    final case object UnknownError extends Error
    final case class UnknownError(error: String) extends Error
  }
}
