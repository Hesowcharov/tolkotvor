package controllers

import javax.inject.{Inject, Singleton}

import models.entities.{Credentials, SessionID, User}
import utils.{CredentialsUtils, UserUtils}
import utils.auth._

import play.api.libs.functional.syntax._
import play.api.mvc._

import cats._
import cats.data.EitherT
import cats.implicits._
import play.api.data.Form
import controllers.templates.PlayTemplates

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
  * Created by hesowcharov on 29.01.17.
  */

@Singleton
class AuthController @Inject() (
  val controllerProvider: GeneralControllerProvider
) extends GeneralController {

  def login = Action.async { implicit request =>

    def parseCredentials(request: Request[AnyContent])
                        (implicit req: Request[_]): EitherT[Future, AuthError, Credentials] = {
      CredentialsUtils.form.bindFromRequest.fold(
        formWithErrors => EitherT.left(
          Future.successful(AuthError.AuthenticateError("Form has error."))
        ),
        credentials => EitherT.right(
          Future.successful(credentials)
        )
      )
    }

    def checkLoggedIn(request: RequestHeader): EitherT[Future, AuthError, Unit] = {
      checkAuthorization(request) match {
        case Some(_) => EitherT.left(
          Future.successful(AuthError.AuthorizationError("User already logged in"))
        )
        case None => EitherT.right(Future.successful(()))
      }
    }

    val sessionId: EitherT[Future, AuthError, SessionID] = for {
      _ <- checkLoggedIn(request)
      creds <- parseCredentials(request)
      sessId <- loginBy(creds)
    } yield sessId

    sessionId.fold(
      err => {
        val msg = err match {
          case AuthError.AuthorizationError(msg) => msg
          case AuthError.AuthenticateError(msg) => msg
          case _ => "Invalid credentials"
        }
        Redirect(routes.AuthController.signIn())
          .flashing("error" -> msg)
      },
      sessionId => {
        Redirect(routes.Application.index())
          .flashing("success" -> "Authentication was successful")
          .withSession("session_id" -> sessionId.id)
      }
    )

  }

  def logout = AuthorizedAction { reqWithUser =>
    val AuthorizedRequest(user, _, request) = reqWithUser    
    val optLogout = for {
      session <- parseSessionID(request)
    } yield for {
      unit <- logoutBy(session)
    } yield unit

    optLogout match {
      case None => Unauthorized.flashing("notice" -> "Logout status: no session_id")
      case Some(logoutRes) =>
        logoutRes.fold(
          authError => Unauthorized.flashing("error" -> "Logout status: user is not logged in (session_id was expired)"),
          success => Unauthorized.flashing("success" -> "Logout status: user is logged out"))
    }
  }

  def register = UserAction.async { implicit req =>
    val UserRequest(optUser, nots, request) = req

    sealed trait Error
    final case class ErrorForm(form: Form[User]) extends Error
    final case object UserExists extends Error

    val errOrForm: Either[Error, User] = UserUtils.registrationForm
      .bindFromRequest
      .fold (
        errForm => Left(ErrorForm(errForm)),
        user => Right(user)
      )

    val result: EitherT[Future, Error, User] = for {
      addingUser <- EitherT(Future.successful(errOrForm))
      addedUser <- usersDao.add(addingUser).leftMap {_ => UserExists: Error }
    } yield addedUser
    result.fold(
      error => error match {
        case ErrorForm(form) =>
          val page = templates.PlayTemplates.main("Registration page")(optUser, nots) {
            templates.PlayTemplates.users.signup(form)
          }
          Ok(page)
        case UserExists =>
          val errorForm = UserUtils.registrationForm.withError("name", "Already used")
          val page = templates.PlayTemplates.main("Registration page")(optUser, nots) {
            templates.PlayTemplates.users.signup(errorForm)
          }
          Ok(page)
      },
      _ => Redirect(routes.Application.index()).flashing("success" -> "Registration was successful")     
    )
  }

  def signIn = UserAction { implicit req =>
    val UserRequest(optUser, nots, request) = req
    val page = PlayTemplates.main("Login page")(optUser, nots) {
      PlayTemplates.users.signin(CredentialsUtils.form)
    }
    Ok(page)
  }

  def signUp = UserAction { implicit req =>
    val UserRequest(optUser, nots, request) = req
    val page = PlayTemplates.main("Registration page")(optUser, nots) {
      PlayTemplates.users.signup(UserUtils.registrationForm)
    }
    Ok(page)
  }

  def userProfile = AuthorizedAction { req =>
    val AuthorizedRequest(user, nots, request) = req
    val page = PlayTemplates.main("User profile")(Option(user), nots) {
      PlayTemplates.users.profile(user)
    }
    Ok(page)
  }
}
