package controllers

import javax.inject.Inject

import models.entities.{Picture, Tag => UserTag, TagPicture, Post, PostContent, User, PictureNotation}
import models.dao.{PicturesDAO, TagsDAO, PostsDAO, NotationsDAO}
import utils._
import play.api.data.Form
import play.api.libs.json.{Json, JsValue}
import play.api.mvc._
import utils.PictureUtils.uploadPictureForm
import com.sksamuel.scrimage._
import com.sksamuel.scrimage.nio._
import cats._
import cats.data.EitherT
import cats.implicits._
import controllers.templates.PlayTemplates

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
  * Created by hesowcharov on 27.01.17.
  */
class PictureController @Inject() (
  val controllerProvider: GeneralControllerProvider,
  val picturesDao: PicturesDAO,
  val postsDao: PostsDAO,
  val tagsDao: TagsDAO,
  val notationsDao: NotationsDAO
) extends GeneralController with DataRequestErrors {

  def uploadFileForm = AuthorizedAction { implicit req =>
    val AuthorizedRequest(user, nots, request) = req
    val page = PlayTemplates.main("Upload new picture")(Some(user), nots) {
      PlayTemplates.pictures.upload_picture(uploadPictureForm)
    }
    Ok(page)
  }

  def uploadPicture = AuthorizedAction.async(parse.multipartFormData) { implicit reqWithUser =>
    import MultipartFormData.FilePart
    import play.api.libs.Files.TemporaryFile

    type PicData = (String, String, String)
    type PicForm = Form[PicData]
    val AuthorizedRequest(user, _, request) = reqWithUser 

    val errOrFile = EitherT.fromOption[Future](request.body.file("picture"), Error.BadRequest)
      .ensure(Error.BadRequest("Wrong image format"))(_.contentType.exists(_.toLowerCase.contains("image")))

    def handleForm: Either[Error, PicData] = uploadPictureForm.bindFromRequest.fold(
      errForm => Left(Error.BadRequest("Invalid filled form fields")),
      form => Right(form)
    )

    def processImage(imageFile: FilePart[TemporaryFile]) = Future {
      imageFile.ref.file.setReadable(true, false)
      val image = Image.fromFile(imageFile.ref.file)
      val scale = image.width.toDouble / image.height
      val resized = if(image.width <= 600 && image.height <= 600) {
        image
      } else if (scale < 1.5) {
        image.scaleTo(600, 480, ScaleMethod.Bicubic)
      } else {
        image.scaleTo(600, 340, ScaleMethod.Bicubic)
      }
      resized
    }

    def moveFile(image: Image, id: Int) = Future {
      val outputFile = new java.io.File(s"$mediaPath$id.jpg")
      image.output(outputFile)(JpegWriter())
      image
    }

    def insertPictureData(name: String, description: String, image: Image): Future[Picture] = {
      import com.github.nscala_time.time.Imports._

      val (width, height) = image.dimensions
      val format = FormatDetector.detect(image.stream).flatMap { format =>
        format match {
          case Format.JPEG => Some("jpg")
          case Format.PNG => Some("png")
          case Format.GIF => Some("gif")
          case _ => None
        }
      }
      val picture = Picture(None, user.id.get, width, height, format, name, description, DateTime.now)
      picturesDao.add(picture)
    }

    def insertTagsData(tagsText: String): Future[Seq[UserTag]] = {
      val tags = PictureUtils
        .parseTags(tagsText)
        .map(t => UserTag(None, t, user.id.get))
      tagsDao.add(tags)
    }

    def insertTagPictureData(pic: Picture, tags: Seq[UserTag]): Future[Seq[TagPicture]] = {
      tagsDao.ref(pic, tags)
    }

    val operationsResult: EitherT[Future, Error, JsValue] = for {
      formData <- EitherT.fromEither[Future](handleForm)
      (name, description, tagsText) = formData
      filepart <- errOrFile
      image <- EitherT.liftT(processImage(filepart))
      picture <- EitherT.liftT(insertPictureData(name, description, image))
      _ <- EitherT.liftT(moveFile(image, picture.id.get))
      tags <- EitherT.liftT(insertTagsData(tagsText))
      tagsPicture <- EitherT.liftT(insertTagPictureData(picture, tags))
    } yield Json.obj("id" -> picture.id)

    operationsResult.fold(
      _ match {
        case Error.BadRequest(error) => BadRequest(error)
        case _ => InternalServerError
      },
      json => Ok(json)
    )
  }


  def showPicture(id: Int) = UserAction.async { implicit req =>

    val UserRequest(optUser, nots, _) = req

    def getPicture(id: Int) = EitherT[Future, Error.NotFound, Picture] {
      picturesDao.getBy(id)
        .map(_.toRight(Error.NotFound("Picture doesn't exist")))
    }

    val operationsResult: EitherT[Future, Error.NotFound, (Picture, Seq[(PictureNotation, Option[(Int, String)])])] = for {
      pic <- getPicture(id)
      nots <- EitherT.liftT(notationsDao.getByPictureId(id))
    } yield (pic, nots)

    operationsResult.fold (
      err => Redirect(routes.Application.index)
        .flashing("error" -> err.error),
      pair => {
        val (pic, notations) = pair
        val notationForm = PlayTemplates.pictures.notation_form(models.forms.Repository.notationForm, Option(pic))
        val page = PlayTemplates.main(s"Review of ${pic.name}")(optUser, nots) {
          PlayTemplates.pictures.picture_review(staticRoute, pic, notations)(notationForm)
        }
        Ok(page)
      }
    )
  }

  def bindWithPost(pictureId: Int, postId: Int) = AuthorizedAction.async { req =>
    val AuthorizedRequest(user, nots, request) = req

    def getPicture(id: Int) = EitherT[Future, Error.NotFound, Picture] {
      picturesDao.getBy(id) map { _.toRight(Error.NotFound("Picture doesn't exist")) }
    }

    def checkPermissionOnPicture(picture: Picture, user: User) = EitherT.fromEither[Future] {
      (picture.uploaderId == user.id.get) match {
        case true => Right(())
        case false => Left(Error.PermissionDenied("User doesn't have permissions on picture"))
      }
    }

    def getPost(id: Int) = EitherT[Future, Error.NotFound, Post] {
      postsDao.getBy(id) map { _.toRight(Error.NotFound("Post doesn't exist")) }
    }

    def checkPermissionOnPost(post: Post, user: User) = EitherT.fromEither[Future] {
      (post.userId == user.id.get) match {
        case true => Right(())
        case false => Left(Error.PermissionDenied("User doesn't have permissions on post"))
      }
    }

    val operationsResult: EitherT[Future, Error, PostContent] = for {
      pic <- getPicture(pictureId)
      _ <- checkPermissionOnPicture(pic, user)
      post <- getPost(postId)
      _ <- checkPermissionOnPost(post, user)
      postContent = PostContent(post.id.get, pic.id, None, None, None)
      binded <- EitherT.liftT(postsDao.add(postContent))
    } yield binded
    operationsResult.fold(
      _ match {
        case Error.NotFound(msg) => NotFound(msg)
        case Error.PermissionDenied(msg) => Forbidden(msg)
        case _ => InternalServerError
      },
      postContent => Ok
    )
  }

  def showPictureFragment(id: Int) = UserAction.async { implicit req =>
    val futErrOrPic = picturesDao.getBy(id).map { _.toRight(Error.NotFound) }
    val operationResult: EitherT[Future, Error.NotFound.type, (Picture, Seq[(PictureNotation, Option[(Int, String)])])] = for {
      pic <- EitherT[Future, Error.NotFound.type, Picture](futErrOrPic)
      nots <- EitherT.liftT(notationsDao.getByPictureId(id))
    } yield (pic, nots)
    operationResult.fold(
      err => NotFound,
      pair => {
        val (pic, nots) = pair
        val notationForm = PlayTemplates.pictures.notation_form(models.forms.Repository.notationForm, Option(pic))
        Ok(PlayTemplates.pictures.picture_review(staticRoute, pic, nots)(notationForm).html)
      }
    )
  }

  def deletePicture(id: Int) = AuthorizedAction.async { req =>
    val AuthorizedRequest(user, nots, _) = req

    def pictureBelongsToRequester(pic: Option[Picture]): EitherT[Future, Error, Picture] = {
      EitherT
        .fromOption[Future](pic, Error.NotFound)
        .ensure(Error.PermissionDenied: Error)(_.uploaderId == user.id.get)
    }
    def removePicture(pic: Picture): EitherT[Future, Error, Unit] = {
      val futRem = picturesDao.remove(pic) map {
        case true => Right(())
        case false => Left(Error.UnknownError)
      }
      EitherT[Future, Error, Unit](futRem)
    }

    val result = for {
      optPic <- EitherT.liftT(picturesDao.getBy(id))
      pic <- pictureBelongsToRequester(optPic)
      success <- removePicture(pic)
    } yield success
    result.fold(
      err => err match {
        case Error.NotFound => Results.NotFound
        case Error.PermissionDenied => Results.Forbidden
        case _ => Results.InternalServerError
      },
      ok => Ok
    )

  }

  def showAllPictures = UserAction.async { implicit req =>
    val UserRequest(optUser, nots, request) = req
    val mainPage = views.html.main
    val futPics = picturesDao.getAllIndependent()
    futPics map { pics =>
      Ok {
        PlayTemplates.main(s"All pictures of Tolkotvor")(optUser, nots) {
          PlayTemplates.pictures.pictures_preview(staticRoute, pics)
        }
      }
    }
  }

  def currentUserPictures = AuthorizedAction.async { req =>
    val AuthorizedRequest(user, nots, _) = req
    for {
      userPics <- picturesDao.getAllIndependentOf(user)
    } yield {
      val page = PlayTemplates.main("User pictures")(Option(user), nots) {
        PlayTemplates.pictures.pictures(userPics, staticRoute, true)
      }
      Ok(page)
    }
  }

  def userPictures(userId: Int) = UserAction.async { req =>
    val UserRequest(optUser, nots, _) = req

    def fetchUser(id: Int) = EitherT[Future, Error, User] {
      usersDao.getBy(id).map { optUser =>
        Either.fromOption(optUser, Error.NotFound("User with this id doesn't exist"))
      }
    }

    val operation: EitherT[Future, Error, (Seq[Picture], Boolean)] = for {
      requiredUser <- fetchUser(userId)
      isOwner = optUser.map(_ == requiredUser).getOrElse(false)
      userPics <- EitherT.right(picturesDao.getAllIndependentOf(requiredUser))
    } yield (userPics, isOwner)

    operation.fold(
      _ match {
        case Error.NotFound(msg) => Redirect(routes.Application.index).flashing("error" -> msg)
        case Error.NotFound => Results.NotFound
        case _ => Results.InternalServerError
      },
      tuple => {
        val (pics, owner) = tuple
        val page = PlayTemplates.main("User pictures")(optUser, nots) {
          PlayTemplates.pictures.pictures(pics, staticRoute, owner)
        }
        Ok(page)
      }
    )
  }
}
