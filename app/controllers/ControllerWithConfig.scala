package controllers

import play.api.Configuration
import play.api.mvc.Controller
import utils.auth._
import play.api.i18n.{I18nSupport, MessagesApi}
import javax.inject.{Singleton, Inject}

/**
  * Created by hesowcharov on 27.01.17.
  */
trait ControllerWithConfig extends Controller {
  val configuration: Configuration  
  protected lazy val mediaPath = configuration.getString("application.media.path").get
  protected lazy val staticRoute = configuration.getString("application.static.route").get
  protected lazy val applicationMode = configuration.getString("application.mode").get
}

trait GeneralController extends ControllerWithConfig
    with AuthComponent
    with I18nSupport {
  def controllerProvider: GeneralControllerProvider
  val configuration: Configuration = controllerProvider.configuration
  def messagesApi: MessagesApi = controllerProvider.messagesApi
  def authProvider: AuthComponentProvider = controllerProvider.authProvider

  require(
    configuration.getString("application.media.path").isDefined,
    "Path to media dir doesn't defined in application.conf (application.media.path)"
  )
  require(
    configuration.getString("application.static.route").isDefined,
    "Route to static files doesn't defined in application.conf (application.static.route)"
  )
  require(
    configuration.getString("application.mode").isDefined,
    "Application mode doesn't defined in application.conf (application.mode)"
  )

  override def AuthorizedAction = if(applicationMode == "development") {
    LoginTestUserAction andThen super.AuthorizedAction
  } else {
    super.AuthorizedAction
  }


}

@Singleton
class GeneralControllerProvider @Inject() (
  val configuration: Configuration,
  val authProvider: AuthComponentProvider,
  val messagesApi: MessagesApi
)

