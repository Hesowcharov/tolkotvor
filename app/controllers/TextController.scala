/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.20
 */

package controllers

import javax.inject.Inject
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import models.entities.{TextData, User, PostContent, Post}
import models.dao.{TextDataDAO, PostsDAO}
import utils.TextDataUtils.publicationForm

import play.api.data.Form
import com.github.nscala_time.time.Imports._
import scala.concurrent.Future
import cats._
import cats.data.EitherT
import cats.implicits._
import play.api.mvc._
import play.api.libs.json._
import controllers.templates.PlayTemplates


class TextController @Inject() (
  val controllerProvider: GeneralControllerProvider,
  val textDataDao: TextDataDAO,
  val postsDao: PostsDAO
) extends GeneralController with DataRequestErrors {

  def allTexts = UserAction.async { reqWithUser =>
    val UserRequest(optUser, _, request) = reqWithUser
    for {
      texts <- textDataDao.getAllIndependent
    } yield {
      val page = PlayTemplates.main("All texts")(optUser) {
        PlayTemplates.texts.texts_preview(texts)
      }
      Ok(page)
    }
  }

  def currentUserTexts = AuthorizedAction.async { req =>
    val AuthorizedRequest(user, nots, _) = req
    for {
      userTexts <- textDataDao.getAllIndependentOf(user)
    } yield {
      val page = PlayTemplates.main("User reviewable texts")(Option(user)) {
        PlayTemplates.texts.texts(userTexts, true)
      }
      Ok(page)
    }
  }

  def userTexts(userId: Int) = UserAction.async { req =>
    val UserRequest(optUser, nots, _) = req

    def fetchUser(id: Int) = EitherT[Future, Error, User] {
      usersDao.getBy(id).map { optUser =>
        Either.fromOption(optUser, Error.NotFound("User with this id doesn't exist"))
      }
    }
    val operation: EitherT[Future, Error, (Seq[TextData], Boolean)] = for {
      requiredUser <- fetchUser(userId)
      isOwner = optUser.map(_ == requiredUser).getOrElse(false)
      userTexts <- EitherT.right(textDataDao.getAllIndependentOf(requiredUser))
    } yield (userTexts, isOwner)

    operation.fold(
      _ match {
        case Error.NotFound(msg) => Redirect(routes.Application.index).flashing("error" -> msg)
        case Error.NotFound => Results.NotFound
        case _ => Results.InternalServerError
      },
      tuple => {
        val (texts, owner) = tuple
        val page = PlayTemplates.main("User reviewable texts")(optUser, nots) {
          PlayTemplates.texts.texts(texts, owner)
        }
        Ok(page)
      }
    )
  }

  def publishText = AuthorizedAction { implicit req =>
    val AuthorizedRequest(user, nots, request) = req
    val page = PlayTemplates.main("Publish new text")(Some(user), nots) {
      PlayTemplates.texts.text_publication(publicationForm)
    }
    Ok(page)
  }

  def postPublishText = AuthorizedAction.async { implicit req =>
    type Data = (Option[String], String)
    type TextForm = Form[Data]
    val errOrForm: Either[Error, Data] = publicationForm.bindFromRequest.fold(
      errForm => Left(Error.BadRequest("Form with error")),
      form => Right(form)
    )
    val AuthorizedRequest(user, nots, request) = req
    val operationsResult: EitherT[Future, Error, Result] = for {
      data <- EitherT.fromEither[Future](errOrForm)
      (optTitle, text) = data
      paragraphs = text.split("\r\n")
      newTextData = TextData(None, user.id.get, optTitle, paragraphs, DateTime.now)
      textData <- EitherT.liftT(textDataDao.add(newTextData))
      answer = Json.obj("id" -> textData.id)
    } yield Ok(answer)
    operationsResult.fold(
      _ match {
        case Error.BadRequest(msg) => BadRequest(msg)
        case _ => InternalServerError
      },
      ok => ok
    )
  }

  def bindWithPost(textId: Int, postId: Int) = AuthorizedAction.async { req =>
    val AuthorizedRequest(user, nots, request) = req

    def getText(id: Int) = EitherT[Future, Error.NotFound, TextData] {
      textDataDao.getBy(id) map { _.toRight(Error.NotFound("Picture doesn't exist")) }
    }

    def checkPermissionOnText(text: TextData, user: User) = EitherT.fromEither[Future] {
      (text.userId == user.id.get) match {
        case true => Right(())
        case false => Left(Error.PermissionDenied("User doesn't have permissions on text"))
      }
    }

    def getPost(id: Int) = EitherT[Future, Error.NotFound, Post] {
      postsDao.getBy(id) map { _.toRight(Error.NotFound("Post doesn't exist")) }
    }

    def checkPermissionOnPost(post: Post, user: User) = EitherT.fromEither[Future] {
      (post.userId == user.id.get) match {
        case true => Right(())
        case false => Left(Error.PermissionDenied("User doesn't have permissions on post"))
      }
    }

    val operationsResult: EitherT[Future, Error, PostContent] = for {
      text <- getText(textId)
      _ <- checkPermissionOnText(text, user)
      post <- getPost(postId)
      _ <- checkPermissionOnPost(post, user)
      postContent = PostContent(post.id.get, None, text.id, None, None)
      binded <- EitherT.liftT(postsDao.add(postContent))
    } yield binded
    operationsResult.fold(
      _ match {
        case Error.NotFound(msg) => NotFound(msg)
        case Error.PermissionDenied(msg) => Forbidden(msg)
        case _ => InternalServerError
      },
      postContent => Ok
    )
  }

  def showText(id: Int) = UserAction.async { implicit req =>
    val UserRequest(optUser, nots, request) = req
    for {
      optText <- textDataDao.getBy(id)
    } yield optText match {
      case Some(text) =>
        val page = PlayTemplates.main(s"Text review ${text.title}")(optUser, nots) {
          PlayTemplates.texts.text_review(text, models.entities.TextNotation.form)
        }
        Ok(page)
      case _ => Redirect(routes.Application.index)
          .flashing("error" -> s"TextData with $id id doesn't exist")
    }
  }

  def showTextFragment(id: Int) = UserAction.async { implicit req =>
    for {
      optText <- textDataDao.getBy(id)
    } yield optText match {
      case Some(text) =>
        Ok(
          PlayTemplates.texts.text_review(text, models.entities.TextNotation.form).html
        )
      case _ => NotFound
    }
  }

  def deleteText(id: Int) = AuthorizedAction.async { req =>
    val AuthorizedRequest(user, nots, request) = req

    def findText(id: Int): EitherT[Future, Error, TextData] = {
      val errOrText = textDataDao.getBy(id) map {
        case Some(txt) => Right(txt)
        case _ => Left(Error.NotFound)
      }
      EitherT[Future, Error, TextData](errOrText)
    }

    def textBelongsToRequester(text: TextData, user: User): EitherT[Future, Error, Unit] = {
      val errOrOk: Either[Error, Unit] = text.userId == user.id.get match {
        case true => Right(())
        case false => Left(Error.PermissionDenied)
      }
      EitherT(Future.successful(errOrOk))
    }

    def removeText(text: TextData): EitherT[Future, Error, Unit] = {
      val errOrOk = textDataDao.remove(text) map {
        case b @ true => Right(())
        case _ => Left(Error.UnknownError)
      }
      EitherT[Future, Error, Unit](errOrOk)
    }
    val result = for {
      text <- findText(id)
      _ <- textBelongsToRequester(text, user)
      _ <- removeText(text)
    } yield ()
    result.fold(
      err => err match {
        case Error.NotFound => Results.NotFound
        case Error.PermissionDenied => Results.Forbidden
        case _ => Results.InternalServerError
      },
      _ => Ok
    )
  }
  
}
