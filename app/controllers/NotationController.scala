package controllers

import javax.inject.{Inject, Singleton}

import models.dao.{NotationsDAO, TextDataDAO, PicturesDAO}
import models.entities.{TextNotation, TextData, Picture, PictureNotation, User}

import play.api.libs.json._
import play.api.libs.functional.syntax._

import play.api.mvc._
import play.api.data.Form
import cats._
import cats.data.EitherT
import cats.implicits._
import com.github.nscala_time.time.Imports._
import utils.DateTimeUtils._
import controllers.templates.PlayTemplates

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
  * Created by hesowcharov on 27.01.17.
  */
@Singleton
class NotationController @Inject() (
  val controllerProvider: GeneralControllerProvider,
  val notationsDao: NotationsDAO,
  val textsDao: TextDataDAO,
  val picturesDao: PicturesDAO
) extends GeneralController with DataRequestErrors {

  def sendPictureNotation(picId: Int) = AuthorizedAction.async(parse.form(models.forms.Repository.notationForm)) { implicit req =>
    val AuthorizedRequest(user, _, request) = req
    val notationData = request.body

    val operationsResult: EitherT[Future, Error, PictureNotation] = for {
      pic <- EitherT[Future, Error, Picture](
        picturesDao.getBy(picId).map(_.toRight(Error.NotFound("Picture not found")))
      )
      picNotation <- EitherT.liftT(notationsDao.addPictureNotation(notationData, picId, user))
    } yield picNotation
    operationsResult.fold(
      err => err match {
        case Error.NotFound(msg) => NotFound(msg)
        case Error.PermissionDenied(msg) => Forbidden(msg)
        case _ => InternalServerError
      },
      notation => {
        val response = Json.obj(
          "pictureId" -> picId,
          "picNotationId" -> notation.id,
          "x" -> notation.xCoord,
          "y" -> notation.yCoord,
          "fragment" -> PlayTemplates.pictures.picture_notation_fragment(notation, Some((user.id.get, user.name))).html.body
        )
        Ok(response)
      }
    )
  }

  def getPictureNotations(id: Int) = Action.async { implicit request =>
    for {
      jsonSeq <- notationsDao.getJsonByPictureId(id)
    } yield {
      val response = Json.obj(
        "status" -> "Ok",
        "notations" -> jsonSeq
      )
      Ok(response)
    }
  }

  def getTextNotations(textId: Int, line: Int) = UserAction.async { req =>
    val UserRequest(optUser, _, request) = req
    for {
      ns <- notationsDao.getByTextId(textId, line)
    } yield {
      implicit val notationWriter: Writes[TextNotation] = (
        (__ \ "text").write[String] and
        (__ \ "created").write[DateTime]
      )(n => (n.notation, n.created))
      val jsNotations = ns.map { case (tn, optU) =>
        Json.obj(
          "notation" -> Json.toJson(tn),
          "userInfo" -> optU.map { u => Json.obj("id" -> u._1, "name" -> u._2) }
        )
      }
      Ok(Json.toJson(jsNotations))
    }
  }

  def sendTextNotation(textId: Int, line: Int) = AuthorizedAction.async { implicit req =>
    val AuthorizedRequest(user, _, request) = req
    type ErrForm = Form[(Int,String)]

    val errFormOrData: EitherT[Future, Form[(Int, String)], (Int, String)] = TextNotation.form.bindFromRequest.fold(
      errForm => EitherT.left(Future.successful(errForm)),
      data => EitherT.right(Future.successful(data))
    )

    def getTextData(id: Int): EitherT[Future, ErrForm, TextData] = {
      val futTextData  = textsDao.getBy(id).map {
        case Some(td) => Either.right(td)
        case None => Either.left(???)
      }
      EitherT(futTextData)
    }

    def insertNotation(notation: TextNotation): EitherT[Future, ErrForm, TextNotation] = {
      val futNot = notationsDao.addTextNotation(notation)
      EitherT.right(futNot)
    }

    val errOrSuccess: EitherT[Future, ErrForm, TextNotation] = for {
      data <- errFormOrData
      (_, comment) = data
      td <- getTextData(textId)
      not = TextNotation(None, td.id.get, user.id.get, line, comment, DateTime.now)
      inserted <- insertNotation(not)
    } yield inserted
    errOrSuccess.fold(
      errForm => Redirect(routes.TextController.showText(textId)).flashing("error" -> "Check form fields"),
      (notation => if(req.isAjax) {
        Ok(Json.obj("text_id" -> textId, "line" -> line, "comment" -> notation.notation))
      } else {
        Redirect(routes.TextController.showText(textId)).flashing("success" -> "Notation was added")
      }
      ))
  }
}
