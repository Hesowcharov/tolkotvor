/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.03.23
 */

package controllers

import javax.inject.{Inject, Singleton}
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import models.dao.{TextDataDAO, PostsDAO}
import models.entities.{PlainText, User, PostContent, Post}

import play.api.mvc._
import play.api.libs.json._
import com.github.nscala_time.time.Imports._
import scala.concurrent.Future
import cats._
import cats.data.EitherT
import cats.implicits._
import play.api.mvc._
import play.api.libs.json._
import controllers.templates.PlayTemplates

@Singleton
class PlainTextController @Inject() (
  val controllerProvider: GeneralControllerProvider,
  val textDataDao: TextDataDAO,
  val postsDao: PostsDAO
) extends GeneralController with DataRequestErrors {
  def showFragment(id: Int) = AuthorizedAction.async { req =>
    val AuthorizedRequest(user, nots, _) = req
    val operation: EitherT[Future, Error, PlainText] = for {
      pText <- EitherT[Future, Error.NotFound.type, PlainText](
        textDataDao.getPlainBy(id).map(_.toRight(Error.NotFound))
      )
      post <- getPost(pText.postId, user.id.get)
    } yield pText
    operation.fold(
      err => err match {
        case Error.NotFound => NotFound
        case Error.NotFound(msg) => NotFound(msg)
        case Error.PermissionDenied(msg) => Forbidden(msg)
        case _ => InternalServerError
      },
      ptext => Ok(PlayTemplates.texts.ptext_edit(ptext).html)
    )
  }

  def add(postId: Int) = AuthorizedAction.async(parse.form(PlainText.form)) { req =>
    val AuthorizedRequest(user, nots, _) = req
    val pText = PlainText(None, postId, req.body, DateTime.now())
    
    val operationResults: EitherT[Future, Error, PlainText] = for {
      post <- getPost(postId, user.id.get)
      inserted <- EitherT.liftT(textDataDao.addPlain(pText))
      postContent = PostContent(postId, None, None, inserted.id, None)
      _ <- EitherT.liftT(postsDao.add(postContent))
    } yield inserted
    operationResults.fold(
      _ match {
        case Error.NotFound(err) => NotFound(err)
        case Error.PermissionDenied(err) => Forbidden(err)
        case _ => InternalServerError
      },
      ptext => Ok(Json.obj("id" -> ptext.id.get))
    )
  }

  private def getPost(postId: Int, userId: Int) = EitherT[Future, Error, Post](
        postsDao.getBy(postId).map(_.toRight(Error.NotFound(s"Post with $postId id doesn't exist")))
      ).ensure(Error.PermissionDenied("User doesn't have permissions on post"))( _.userId == userId )

  def delete(id: Int) = AuthorizedAction.async { req =>
    val AuthorizedRequest(user, nots, _) = req
    val operationsResult: EitherT[Future, Error, Unit] = for {
      text <- EitherT[Future, Error, PlainText] {
        textDataDao.getPlainBy(id).map(_.toRight(Error.NotFound(s"Text with $id id doesn't exist")))
      }
      post <- getPost(text.postId, user.id.get)
      unit <- EitherT[Future, Error, Unit](
        textDataDao.removePlain(text).map { deleted =>
          if(deleted) Right(())
          else Left(Error.UnknownError)
        }
      )
    } yield unit
    operationsResult.fold(
      _ match {
        case Error.PermissionDenied(err) => Forbidden(err)
        case Error.NotFound(err) => NotFound(err)
        case _ => InternalServerError
      },
      _ => Ok
    )
  }

}
