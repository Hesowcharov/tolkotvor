package controllers

import play.api._
import play.api.mvc._
import javax.inject.Inject
import controllers.templates.PlayTemplates

import models.dao._

class Application @Inject() (
  val controllerProvider: GeneralControllerProvider
) extends GeneralController {

  def index = UserAction { implicit request =>
    val UserRequest(optUser, nots, req) = request
    val page = PlayTemplates.main("Welcome")(optUser, nots)(
      PlayTemplates.index()
    )
    Ok(page)
  }

}
