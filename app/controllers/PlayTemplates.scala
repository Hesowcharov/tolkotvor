/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.03.29
 */
package controllers.templates

import play.twirl.api.Html
import play.api.data.Form
import play.api.mvc._
import play.api.i18n.Messages

import models.entities._
import utils.Notification

case class PlayTemplate(html: Html, css: Seq[String] = Seq.empty, js: Seq[String] = Seq.empty)

object PlayTemplates {
  import views.{html => H}

  def union(medias: Seq[String]*): Seq[String] = medias.flatten.distinct

  object pictures {
    import views.html.{pictures => H}

    def notation_form(notationForm: Form[(Int, Int, String)], picture: Option[Picture] = None)(implicit messages: Messages, headers: RequestHeader): PlayTemplate = {
      PlayTemplate(
        H.notation_form(notationForm, picture),
        Seq("picture.css", "form.css")
      )
    }

    def picture_review(route: String, picture: Picture, notations: Seq[(PictureNotation,Option[(Int, String)])])
                      (notationForm: PlayTemplate)
                      (implicit messages: Messages): PlayTemplate = {
      PlayTemplate(
        H.picture_review(route, picture, notations)(notationForm.html),
        union(Seq("picture.css"), notationForm.css),
        union(Seq("picture-handler.js"), notationForm.js)
      )
    }

    def pictures_preview(route: String, pictures: Seq[Picture]) = PlayTemplate(
      H.pictures_preview(route, pictures)
    )

    def pictures(pics: Seq[Picture], staticRoute: String, isOwner: Boolean): PlayTemplate = {
      PlayTemplate(
        H.pictures(pics, staticRoute, isOwner),
        Seq("table.css")
      )
    }

    def picture_notation_fragment(notation: PictureNotation, optUserInfo: Option[(Int, String)]) = PlayTemplate (
      H.picture_notation_fragment(notation, optUserInfo)
    )

    def upload_picture(uploadPictureForm: Form[(String, String, String)], post: Option[Post] = None)(implicit messages: Messages, headers: RequestHeader) = PlayTemplate(
      H.upload_picture(uploadPictureForm, post),
      Seq("form.css"),
      Seq("upload_picture.js")
    )
  }

  object posts {
    import views.html.{posts => H}

    def edit_post
    (picForm: Form[(String, String, String)], textForm: Form[(Option[String], String)])
    (route: String, post: Post, content: Seq[PostFragment], notationForm: Form[(Int, String)])(implicit messages: Messages, headers: RequestHeader): PlayTemplate = {
      PlayTemplate(
        H.edit_post(picForm, textForm)(route, post, content, notationForm),
        Seq("picture.css", "text.css", "post.css", "form.css", "comment_form.css"),
        Seq("post-creating.js", "picture-handler.js", "text-handling.js")
      )
    }

    def post_review(route: String, postWrapper: PostWrapper, textNotationForm: Form[(Int, String)])(implicit messages: Messages, header: RequestHeader) = PlayTemplate(
      H.post_review(route, postWrapper, textNotationForm),
      Seq("picture.css", "text.css", "post.css", "form.css", "comment_form.css"),
      Seq("picture-handler.js", "text-handling.js")
    )

    def post_preview(postWrapper: PostWrapper, route: String)(implicit messages: Messages): PlayTemplate = {
      PlayTemplate(
        H.post_preview(postWrapper, route),
        Seq("picture.css", "text.css", "post.css", "form.css", "comment_form.css"),
        Seq("picture-handler.js", "text-handling.js")
      )
    }

    def posts(posts: Seq[PostWrapper], route: String)(implicit messages: Messages): PlayTemplate = {
      PlayTemplate(
        H.posts(posts, route),
        Seq("picture.css", "text.css", "post.css")
      )
    }

    def review_posts(posts: Seq[Post], owner: Boolean): PlayTemplate = {
      PlayTemplate(
        H.review_posts(posts, owner),
        Seq("table.css")
      )
    }
  }

  object texts {
    import views.html.{texts => H}

    def text_notation_form(id: Int, form: Form[(Int, String)])
                          (implicit messages: Messages, headers: RequestHeader) = PlayTemplate(
      H.text_notation_form(id, form),
      Seq(),
      Seq("text-handling.js")
    )

    def text_publication(textPublicationForm: Form[(Option[String], String)])(implicit messages: Messages, header: RequestHeader) = PlayTemplate(
      H.text_publication(textPublicationForm),
      Seq("form.css"),
      Seq("text_publication.js")
    )

    def text_review(text: TextData, form: Form[(Int, String)])(implicit messages: Messages, headers: RequestHeader) = PlayTemplate(
      H.text_review(text, form),
      Seq("form.css", "text.css", "comment_form.css"),
      Seq("text-handling.js")
    )

    def texts_preview(texts: Seq[TextData]) = PlayTemplate(
      H.texts_preview(texts),
      Seq("text.css")
    )

    def texts(texts: Seq[TextData], isOwner: Boolean) = PlayTemplate(
      H.texts(texts, isOwner),
      Seq("table.css")
    )

    def ptext_edit(text: PlainText): PlayTemplate = {
      PlayTemplate(
        H.ptext_edit(text)
      )
    }

    def ptext_form(post: Post)(implicit messages: Messages, headers: RequestHeader) = PlayTemplate(
      H.ptext_form(post),
      Seq("form.css")
    )
  }

  object users {
    import views.html.{users => H}

    def profile(user: User) = PlayTemplate(
      H.profile(user)
    )

    def signin(loginForm: Form[Credentials])(implicit messages: Messages, headers: RequestHeader) = PlayTemplate(
      H.signin(loginForm)
    )

    def signup(regForm: Form[User])(implicit messages: Messages, headers: RequestHeader) = PlayTemplate(
      H.signup(regForm),
      Seq("form.css")
    )
  }

  def index() = PlayTemplate (
    H.index()
  )

  def main(title: String)
    (optUser: Option[User], notifications: Seq[Notification] = Seq.empty[Notification])
    (templates: PlayTemplate*): Html = {
    val css = ("main.css" +: templates.flatten(_.css)).distinct
    val js = ("main.js" +: templates.flatten(_.js)).distinct
    val htmls =  templates.map(_.html).to[collection.immutable.Seq]
    val html = play.twirl.api.HtmlFormat.fill(htmls)
    H.main(title)(css, js)(optUser, notifications)(html)
  }
}
