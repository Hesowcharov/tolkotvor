package controllers

import javax.inject.Inject

import models.entities.User
import models.dao.UsersDAO
import utils.UserUtils._
import controllers.templates.PlayTemplates

import play.api.libs.json.Json
import play.api.mvc._

import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
  * Created by hesowcharov on 27.01.17.
  */
class UserController @Inject() (
  val controllerProvider: GeneralControllerProvider
) extends GeneralController {

  def getAllUsers = Action.async {
    import utils.UserUtils._
    usersDao.getAll().map { us => Ok(Json.toJson(us)) }
  }

  def show(id: Int) = UserAction.async { req =>
    val UserRequest(user, nots, request) = req
    for {
      optUser <- usersDao.getBy(id)
    } yield {
      optUser.map { requestedUser =>
        val page = PlayTemplates.main("User profile")(user, nots) {
          PlayTemplates.users.profile(requestedUser)
        }
        Ok(page)
      }.getOrElse {
        play.api.mvc.Results.Redirect(routes.Application.index)
          .flashing("error" -> "User doesn't exist")
      }
    }
  }
}
