/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.11
 */

package utils.auth

object UUID {
  def generateUUID(): String = {
    java.util.UUID.randomUUID.toString
  }
}
