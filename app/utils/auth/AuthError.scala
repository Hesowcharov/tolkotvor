/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.11
 */

package utils.auth

sealed trait AuthError

object AuthError {
  final case class AuthenticateError(info: String) extends AuthError
  final case class AuthorizationError(info: String) extends AuthError
  final case object UserNotFound extends AuthError
  final case object UserExists extends AuthError
}


