/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.11
 */

package utils.auth

import javax.inject.{Inject, Singleton}

import cats.data.EitherT
import cats.implicits._
import controllers.routes
import models.dao.UsersDAO
import models.entities.{Credentials, SessionID, User}
import play.api.cache._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc._
import utils.{CacheStorage, Notification}

import scala.concurrent.Future
import scala.concurrent.duration._

@Singleton
class AuthComponentProvider @Inject() (
  val cache: CacheApi, 
  val usersDao: UsersDAO) {
}

trait AuthComponent extends AuthHandler {
  def authProvider: AuthComponentProvider
  def cache = authProvider.cache
  def usersDao = authProvider.usersDao

  trait AjaxTestable { this: RequestHeader =>
    def isAjax = this.headers.get("X-Requested-With").isDefined
  }

  case class UserRequest[A](user: Option[User],  notifications: Seq[Notification], request: Request[A])
      extends WrappedRequest[A](request) with AjaxTestable {
    
  }

  case class AuthorizedRequest[A](user: User, notifications: Seq[Notification], request: Request[A])
      extends WrappedRequest(request) with AjaxTestable

  object LoginTestUserAction extends ActionBuilder[Request] {
    def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]) = {
      if (checkAuthorization(request).isEmpty) {
        loginBy(Credentials("test", "12345678")).fold[Future[Result]](
          _ => Future.successful { Results.Forbidden("Create test user with 'test' and '12345678' credentials") },
          sessionId => block(request).map{ _.withSession("session_id" -> sessionId.id) }
        ).flatten
      } else {
         block(request)
      }
    }
  }

  object UserAction extends ActionBuilder[UserRequest]
      with ActionTransformer[Request, UserRequest] {
    def transform[A](request: Request[A]) = Future.successful {
      val optUser = checkAuthorization(request)
      val nots = request.flash.data.toSeq.map { case (t, m) =>
        t match {
          case "success" => Notification.Success(m)
          case "error" => Notification.Error(m)
          case "notice" => Notification.Notice(m)
        }
      }
      UserRequest(optUser, nots, request)
    }
  }

  object CheckAuthorizationAction extends ActionRefiner[UserRequest, AuthorizedRequest] {
    override def refine[A](request: UserRequest[A]) = Future.successful {
      val UserRequest(optUser, nots, req) = request
      optUser match {
        case Some(user) => Right(AuthorizedRequest(user, nots, req))
        case _ if( request.isAjax ) => Left(Results.Forbidden)
        case _ => Left(
          Results.Redirect(routes.AuthController.signIn())
            .flashing("error" -> "You aren't authorized.")
        )
      }
    }
  }

  object ExtendExpiringTimeAction extends ActionRefiner[AuthorizedRequest, AuthorizedRequest] {
    override def refine[A](request: AuthorizedRequest[A]) = Future.successful {
      val sessionId = parseSessionID(request).get
      extendExpiringTime(sessionId)
      Right(request)
    }
  }

  def AuthorizedAction = UserAction andThen CheckAuthorizationAction andThen ExtendExpiringTimeAction
}

trait AuthHandler
    extends AuthService
    with CacheStorage[User] {

  def expiryTime = 30 minutes

  def checkAuthorization(request: RequestHeader): Option[User] = {
    for {
      sessionID <- parseSessionID(request)
      user <- isAuthorized(sessionID)
    } yield user
  }

  def parseSessionID(request: RequestHeader): Option[SessionID] = {
    request.session
      .get("session_id")
      .map(SessionID(_))
  }

}

trait AuthService { this: CacheStorage[User]  =>
  def usersDao: UsersDAO

  def isAuthorized(sessionID: SessionID): Option[User] = {
    for {
      user <- getFromCache[User]("session." + sessionID.id)
    } yield user
  }

  def loginBy(credentials: Credentials): EitherT[Future, AuthError, SessionID] = {
    for {
      user <- usersDao.getBy(credentials)
    } yield {
      val uuid = UUID.generateUUID()
      setInCache(s"session.$uuid", user)
      SessionID(uuid)
    }
  }

  def logoutBy(sessionId: SessionID): Either[AuthError, Unit] = {
    val SessionID(id) = sessionId
    val key = s"session.$id"
    getFromCache(key) match {
      case Some(_) => Either.right(removeFromCache(key))
      case _ => Either.left(AuthError.AuthorizationError("User wasn't logged in."))
    }
  }

  def extendExpiringTime(sessionID: SessionID): Either[AuthError, Unit] = {
    val key = s"session.${sessionID.id}"
    getFromCache[User](key) match {
      case Some(usr) => Right(setInCache(key, usr))
      case _ => Left(AuthError.UserNotFound)
    }
  }
}

