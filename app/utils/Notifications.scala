/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.18
 */

package utils

sealed trait Notification extends Ordered[Notification] {
  import Notification._

  val message: String
  def compare(that: Notification): Int = (this, that) match {
    case (Error(_), Success(_)) => 1
    case (Success(_), Error(_)) => -1
    case (Success(_), Notice(_)) => 1
    case (Notice(_), Success(_)) => -1
    case (Error(_), Notice(_)) => 1
    case (Notice(_), Error(_)) => -1
    case (Error(m1), Error(m2)) => m1 compareTo m2
    case (Notice(m1), Notice(m2)) => m1 compareTo m2
    case (Success(m1), Success(m2)) => m1 compareTo m2
  }
}

object Notification {
  final case class Error(message: String) extends Notification
  final case class Notice(message: String) extends Notification
  final case class Success(message: String) extends Notification
}

