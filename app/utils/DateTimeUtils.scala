/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.04.06
 */

package utils

import com.github.nscala_time.time.Imports._
import play.api.libs.json.{Reads, Writes}

object DateTimeUtils {
  val pattern = "Y-MM-dd k:m:s"
  val formatter = DateTimeFormat.forPattern(pattern)
  implicit val dtReads = Reads.jodaDateReads(pattern)
  implicit val dtWrites = Writes.jodaDateWrites(pattern)
}
