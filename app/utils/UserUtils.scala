/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.11
 */

package utils

import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import models.entities.{ User, Credentials }
import com.github.nscala_time.time.Imports._
import DateTimeUtils._

object UserUtils {
  import play.api.data._
  import play.api.data.Forms._

  implicit val newUserReads: Reads[User] = (
      (__ \ "name").read[String] and
      (__ \ "email").readNullable[String] and
      (__ \ "password").read[String]
    ) ((n, e, p) => User(None, n, e, p, DateTime.now))

  implicit val writes: Writes[User] = (
      (__ \ "id").writeNullable[Int] and
      (__ \ "name").write[String] and
      (__ \ "email").writeNullable[String] and
      (__ \ "password").write[String] and
      (__ \ "registered").write[DateTime]
  ) ( user => (user.id, user.name, user.email, user.password, user.registered))

  val registrationForm = Form(
    mapping(
        "name" -> nonEmptyText,
        "email" -> optional(email),
        "password" -> nonEmptyText(8, 20)
      ) ( (n, e, p) => User(None, n, e, p, DateTime.now))
        ( u => Some((u.name, u.email, u.password)) )
    )
    
}

object CredentialsUtils {
  import play.api.data._
  import play.api.data.Forms._

  implicit val validStructure: Reads[Credentials] = (
    (__ \ "name").read[String] and
      (__ \ "password").read[String]
  ) (Credentials.apply _)

  val form: Form[Credentials] = Form(
    mapping(
      "name" -> nonEmptyText,
      "password" -> nonEmptyText
    )(Credentials.apply) (Credentials.unapply)
  )

}
