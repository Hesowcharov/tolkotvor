/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.20
 */

package utils

import play.api.data._
import play.api.data.Forms._

object TextDataUtils {
  val publicationForm = Form(
    tuple(
      "title" -> optional(text),
      "text" -> nonEmptyText
    )
  )
}
