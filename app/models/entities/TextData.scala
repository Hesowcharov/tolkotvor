/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.20
 */

package models.entities

import com.github.nscala_time.time.Imports._

case class TextData(
  id: Option[Int] = None,
  userId: Int,
  title: Option[String],
  text: Array[String],
  created: DateTime
) extends PostFragment
