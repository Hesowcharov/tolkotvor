/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.05.14
 */
package models.entities

import com.github.nscala_time.time.Imports._
import models.dao.{NotationsDAO, PicturesDAO, PostsDAO, TextDataDAO}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import javax.inject.{Inject, Singleton}

import scala.concurrent.Future

case class PostContent(
  postId: Int,
  pictureId: Option[Int],
  textId: Option[Int],
  plainTextId: Option[Int],
  order: Option[Int]
)

trait PostFragment {
  def created: DateTime
}

case class OrderedPostFragment(fragment: PostFragment, order: Int)

case class PostWrapper(post: Post, fragments: Seq[PostFragment])

@Singleton
class DaoRepo @Inject() (val pictures: PicturesDAO,
                         val texts: TextDataDAO,
                         val notations: NotationsDAO,
                         val posts: PostsDAO)

class PostWrapperUtils @Inject() (daoRepo: DaoRepo) {

  def build(post: Post): Future[PostWrapper] = {
    for {
      pics <- daoRepo.pictures.getBy(post)
      notations <- Future.sequence(
        pics map { p => daoRepo.notations.getByPictureId(p.id.get) }
      )
      postContent <- daoRepo.posts.getContentOf(post)
      picReviews = pics zip notations map { case (p, ns) => PictureReview(p, ns) }
      texts <- daoRepo.texts.getBy(post)
      ptexts <- daoRepo.texts.getPlainBy(post)
    } yield {
      val (orderedContent, unorderedContent) = postContent.partition(_.order.isDefined)
      val fragmentsWithOrder = orderedContent map { pc =>
        pc match {
          case PostContent(_, optPicId@Some(_), _, _, Some(order)) =>
            val Some(pic) = picReviews.find(_.picture.id == optPicId)
            OrderedPostFragment(pic, order)
          case PostContent(_, _, optTextId@Some(_), _, Some(order)) =>
            val Some(text) = texts.find(_.id == optTextId)
            OrderedPostFragment(text, order)
          case PostContent(_, _, _, optPTextId@Some(_), Some(order)) =>
            val Some(ptext) = ptexts.find(_.id == optPTextId)
            OrderedPostFragment(ptext, order)
        }
      }

      val orderedFragments = fragmentsWithOrder
        .sortBy(_.order)
        .map(_.fragment)

      val fragmentsWithoutOrder = unorderedContent map { pc =>
        pc match {
          case PostContent(_, optPicId@Some(_), _, _, _) =>
            val Some(pic) = picReviews.find(_.picture.id == optPicId)
            pic
          case PostContent(_, _, optTextId@Some(_), _, _) =>
            val Some(text) = texts.find(_.id == optTextId)
            text
          case PostContent(_, _, _, optPTextId@Some(_), _) =>
            val Some(ptext) = ptexts.find(_.id == optPTextId)
            ptext
        }
      }
      val unorderedFragments = fragmentsWithoutOrder.sortBy(_.created)
      val fragments = orderedFragments ++ unorderedFragments
      PostWrapper(post, fragments)
    }
  }

  def delete(post: Post): Future[Unit] = {
    for {
      wrapper <- build(post)
    } yield wrapper.fragments.foreach { f => f match {
      case td: TextData => daoRepo.texts.remove(td)
      case picReview: PictureReview => daoRepo.pictures.remove(picReview.picture)
      case ptext: PlainText => daoRepo.texts.removePlain(ptext)
      case _ =>
    }}
  }
}
