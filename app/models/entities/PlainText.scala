/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.03.24
 */

package models.entities

import play.api.data._
import play.api.data.Forms._
import com.github.nscala_time.time.Imports._

case class PlainText (
  id: Option[Int],
  postId: Int,
  text: String,
  created: DateTime
) extends PostFragment

object PlainText {

  def tupled = (PlainText.apply _).tupled

  val form = Form(
    single(
      "text" -> nonEmptyText
    )
  )
}
