package models.entities

import com.github.nscala_time.time.Imports._

/**
  * Created by hesowcharov on 23.01.17.
  */
case class Picture(
  id: Option[Int] = None,
  uploaderId: Int,
  width: Int,
  height: Int,
  format: Option[String],
  name: String,
  description: String,
  uploaded: DateTime
)

case class PictureReview(picture: Picture, notation: Seq[(PictureNotation, Option[(Int, String)])]) extends PostFragment {
  def created = picture.uploaded
}
