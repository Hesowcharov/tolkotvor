/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.28
 */

package models.entities

import com.github.nscala_time.time.Imports._
import play.api.data.Form
import play.api.data.Forms._

case class TextNotation(
  id: Option[Int] = None,
  textID: Int,
  userID: Int,
  line: Int,
  notation: String,
  created: DateTime
)

object TextNotation {
  val form = Form(
    tuple(
      "line-number" -> number,
      "comment" -> nonEmptyText
    )
  )

  def tupled = (TextNotation.apply _).tupled
}
