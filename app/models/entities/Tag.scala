/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.19
 */

package models.entities

case class Tag(id: Option[Int] = None, name: String, initiatorId: Int) {
  override def equals(that: Any): Boolean = {
    that match {
      case Tag(_, name, id) => this.name == name && this.initiatorId == id
      case _ => false
    }
  }
}
