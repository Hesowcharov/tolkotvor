package models.entities

/**
  * Created by hesowcharov on 27.01.17.
  */

import com.github.nscala_time.time.Imports._

case class User(
  id: Option[Int] = None,
  name: String,
  email: Option[String],
  password: String,
  registered: DateTime
)
