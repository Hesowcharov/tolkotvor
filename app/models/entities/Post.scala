/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.03.03
 */

package models.entities

import com.github.nscala_time.time.Imports._

case class Post(
  id: Option[Int],
  userId: Int,
  title: String,
  created: DateTime,
  updated: DateTime
)
