package models.dao

import javax.inject.{Inject, Singleton}

import models.entities.{PictureNotation, User, TextNotation}
import models.Tables
import slick.driver.JdbcProfile
import slick.driver.PostgresDriver.api._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import com.github.nscala_time.time.Imports._

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json._
import play.api.libs.functional.syntax._

/**
  * Created by hesowcharov on 26.01.17.
  */

@Singleton
class NotationsDAO @Inject() (
  protected val dbConfigProvider: DatabaseConfigProvider,
  protected val picturesDao: PicturesDAO
)
    extends HasDatabaseConfigProvider[JdbcProfile] {

  import Tables.{pictureNotations, textNotations, textDatas, users}

  def addPictureNotation(essentialData: (Int, Int, String), pictureId: Int, user: User): Future[PictureNotation] = {
    
    val (xCoord, yCoord, comment) = essentialData
    val created = DateTime.now
    val notation = PictureNotation(None, pictureId, user.id.get, xCoord, yCoord, comment, created)
    val insertQuery = pictureNotations returning  pictureNotations.map(_.id) into (
      (picNot, newId) => picNot.copy(id = Option(newId))
    )
    db.run(insertQuery += notation)
  }

  def getByPictureId(id: Int): Future[Seq[(PictureNotation, Option[(Int, String)])]] = {
    val query = pictureNotations
      .filter(_.pictureId === id)
      .joinLeft(users).on(_.userId === _.id)
      .map { case (n, u) => (n, u.map( uu => (uu.id, uu.name)) )  }
      .result
    db.run(query)
  }

  def getJsonByPictureId(id: Int): Future[Seq[JsObject]] = {
    import utils.NotationUtils._
    getByPictureId(id).map { seq =>
      seq.map { case (n, optU) => Json.obj(
        "notation" -> Json.toJson(n),
        "userInfo" -> optU.map { u => Json.obj("id" -> u._1, "name" -> u._2) }
      ) }
    }
  }

  def addTextNotation(notation: TextNotation): Future[TextNotation] = {
    val insertTextNotationsSchema = textNotations returning textNotations.map(_.id) into (
      (not, newId) => not.copy(id = Option(newId)))
    db.run(insertTextNotationsSchema += notation)
  }

  def getByTextId(id: Int, line: Int): Future[Seq[(TextNotation, Option[(Int, String)])]] = {
    val query = for {
      (n, u) <- textNotations joinLeft users on (_.userId === _.id) if (n.textId === id && n.line === line)
    } yield (n, u.map( us => (us.id, us.name)))
    db.run(query.result)
  }

}
