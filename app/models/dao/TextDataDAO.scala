/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.02.20
 */

package models.dao

import javax.inject.{Inject, Singleton}
import slick.driver.JdbcProfile
import slick.driver.PostgresDriver.api._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import utils.ColumnTypeMappers._

import models.entities.{TextData, PlainText, Post, User}

@Singleton
class TextDataDAO @Inject() (
  protected val dbConfigProvider: DatabaseConfigProvider
) extends HasDatabaseConfigProvider[JdbcProfile] {
  import models.Tables.{textDatas, postContents, plainTexts}

  val baseQuery = textDatas.sortBy(_.created.desc)

  val insertScheme = textDatas returning textDatas.map(_.id) into (
    (text, newId) => text.copy(id = Option(newId))
  )
  val plainInsertScheme = plainTexts returning plainTexts.map(_.id) into (
    (ptext, newId) => ptext.copy(id = Option(newId))
  )

  def getAll(): Future[Seq[TextData]] = db.run(textDatas.result)

  def getAll(user: User): Future[Seq[TextData]] = {
    val query = textDatas.filter(_.userId === user.id)
    db.run(query.result)
  }

  def getAllIndependent(): Future[Seq[TextData]] = {
    val query = for {
      (td, pc) <- baseQuery
        .joinLeft(postContents).on(_.id === _.textId) if pc.map(_.textId).isEmpty
    } yield td
    db.run(query.result)
  }

  def getAllIndependentOf(user: User): Future[Seq[TextData]] = {
    val query = for {
      (td, pc) <- baseQuery
      .joinLeft(postContents).on(_.id === _.textId) if pc.map(_.textId).isEmpty
        if (td.userId === user.id.get)
    } yield td
    db.run(query.result)
  }


  def add(text: TextData): Future[TextData] = db.run(insertScheme += text)

  def getBy(id: Int): Future[Option[TextData]] = {
    val query = textDatas
      .filter( _.id === id)
      .result
      .headOption
    db.run(query)
  }

  def getBy(post: Post): Future[Seq[TextData]] = {
    val query = for {
      (pc, t) <- postContents.filter(_.postId.? === post.id) join textDatas on (_.textId === _.id )
    } yield t
    db.run(query.result)
  }

  def remove(text: TextData): Future[Boolean] = {
    val query = textDatas.filter(_.id === text.id)
    db.run(query.delete).map {
      case num if (num > 0) => true
      case _ => false
    }
  }

  def addPlain(ptext: PlainText): Future[PlainText] = db.run(plainInsertScheme += ptext)

  def getPlainBy(id: Int): Future[Option[PlainText]] = {
    val query = plainTexts
      .filter(_.id === id)
      .result
      .headOption
    db.run(query)
  }

  def getPlainBy(post: Post): Future[Seq[PlainText]] = {
    val query = for {
      (_, pt) <- postContents.filter(_.postId.? === post.id) join plainTexts on (_.plainTextId === _.id)
    } yield pt
    db.run(query.result)
  }

  def removePlain(ptext: PlainText): Future[Boolean] = {
    val query = plainTexts.filter(_.id.? === ptext.id)
    db.run(query.delete).map {_ > 0 }
  }

}
