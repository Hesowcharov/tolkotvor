/*
 * Created by hesowcharov / Dmitry Ovcharov on 2017.03.05
 */

package models.dao

import models.Tables
import models.entities.{Post, PostContent, User}

import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import slick.driver.PostgresDriver.api._
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import utils.ColumnTypeMappers._


@Singleton
class PostsDAO @Inject() (
  val dbConfigProvider: DatabaseConfigProvider
) extends HasDatabaseConfigProvider[JdbcProfile] {
  import Tables.{posts, postContents}

  def getBy(id: Int): Future[Option[Post]] = {
    val query = posts.filter(_.id === id).result.headOption
    db.run(query)
  }

  def getContentOf(post: Post): Future[Seq[PostContent]] = {
    val query = postContents filter(_.postId.? === post.id)
    db.run(query.result)
  }

  def getAllOf(user: User): Future[Seq[Post]] = {
    val query = posts.filter(_.userId === user.id.get).sortBy(_.updated.desc)
    db.run(query.result)
  }

  def getAll: Future[Seq[Post]] = {
    val query = posts.sortBy(_.created.desc)
    db.run(query.result)
  }

  def add(post: Post): Future[Post] = {
    val insertQuery = posts returning posts.map(_.id) into (
      (post, newId) => post.copy(id = Some(newId))
    )
    db.run(insertQuery += post)
  }

  def delete(post: Post): Future[Boolean] = {
    val query = posts.filter(_.id.? === post.id)
    db.run(query.delete).map {
      case num if (num > 0) => true
      case _ => false
    }
  }

  def add(postContent: PostContent): Future[PostContent] = {
    val insertQuery = postContents returning postContents
    db.run(insertQuery += postContent)
  }

  def update(post: Post): Future[Boolean] = {
    val updateQuery = for {
      p <- posts if (p.id === post.id)
    } yield p
    db.run(updateQuery.update(post)) map (_ > 0)
  }

  def updateOrder(contents: Seq[PostContent]): Future[Boolean] = {
    val queries = contents.map { cont =>
      postContents
        .filter( pc =>
          pc.picId.? === cont.pictureId ||
          pc.textId.? === cont.textId ||
          pc.plainTextId.? === cont.plainTextId
        )
        .map(_.order)
        .update(cont.order.get)
    }
    db.run(DBIO.sequence(queries))
      .map(_.nonEmpty)
  }
}
