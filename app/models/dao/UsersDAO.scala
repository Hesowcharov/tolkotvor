package models.dao

import play.api.db.slick.{HasDatabaseConfigProvider, DatabaseConfigProvider}
import slick.driver.JdbcProfile
import slick.driver.PostgresDriver.api._

import com.github.nscala_time.time.Imports._

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import cats._
import cats.data.EitherT
import cats.implicits._

import models.entities.{ User, Credentials }
import models.Tables
import utils.ColumnTypeMappers._
import utils.auth._
import com.github.t3hnar.bcrypt._

/**
  * Created by hesowcharov on 27.01.17.
  */

@Singleton
class UsersDAO @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import Tables.users

  def getAll(): Future[Seq[User]] = db.run(users.result)

  def exists(userName: String): Future[Boolean] = {
    val query = users.filter(_.name === userName)
    db.run(query.result.headOption).map(_.isDefined)
  }

  def getBy(credentials: Credentials): EitherT[Future, AuthError, User] = {
    val Credentials(name, pwd) = credentials
    val query = for {
      user <- users if (user.name === name)
        } yield user
    val errOrUser: Future[Either[AuthError, User]] = db.run(query.result.headOption).map {
      case Some(usr) => Right(usr)
      case _ => Left(AuthError.UserNotFound)
    }
    EitherT(errOrUser).ensure(AuthError.AuthenticateError("Invalid credentials"))(usr =>
      credentials.password.isBcrypted(usr.password)
    )

  }

  def getBy(id: Int): Future[Option[User]] = {
    val query = for {
      user <- users if (user.id === id)
        } yield user
    db.run(query.result.headOption)
  }

  def add(user: User): EitherT[Future, AuthError, User] = {

    val cred = Credentials(user.name, user.password)
    val query = users returning users.map(_.id) into
      ( (user, createdId) => user.copy(id = Some(createdId)) )

    def checkUnique(u: User): EitherT[Future, AuthError, Unit] = {
      val errOrOk: Future[Either[AuthError, Unit]] = exists(u.name).map {
        case true => Left( AuthError.UserExists )
        case false => Right( () )
      }
      EitherT(errOrOk)
    }

    for {
      _ <- checkUnique(user)
      encodedUser = user.copy(password = user.password.bcrypt)
      addedUser <- EitherT.liftT(db.run(query += encodedUser))
    } yield addedUser

  }

  def delete(user: User): Future[Unit] = {
    val query = for {
      deleting <- users if (deleting.id === user.id)
    } yield deleting
    db.run(query.delete).map(_ => ())
  }

}
