package models.dao

import javax.inject.{Inject, Singleton}

import com.github.nscala_time.time.Imports._
import models.entities.{Picture, Post, PostContent, User}
import slick.driver.JdbcProfile
import slick.driver.PostgresDriver.api._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import models.Tables

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import utils.ColumnTypeMappers._

/**
  * Created by hesowcharov on 26.01.17.
  */


@Singleton
class PicturesDAO @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)
    extends  HasDatabaseConfigProvider[JdbcProfile] {

  import Tables.{pictures, postContents, users}

  val baseQuery = pictures.sortBy(_.uploaded.desc)

  def add(picture: Picture): Future[Picture] = {
    val insertScheme = pictures returning pictures.map(_.id) into ((pic, newId) => pic.copy(id = Option(newId)))
    db.run(insertScheme += picture)
  }

  def getAll(): Future[Seq[Picture]] = db.run(pictures.result)

  def getAllIndependent(): Future[Seq[Picture]] = {
    val query = for {
      (p, pc) <- baseQuery
        .joinLeft(postContents).on(_.id === _.picId) if pc.map(_.picId).isEmpty
    } yield p
    db.run(query.result)
  }

  def getAllIndependentOf(user: User): Future[Seq[Picture]] = {
    val query = for {
      (p, pc) <- baseQuery
      .joinLeft(postContents).on(_.id === _.picId) if pc.map(_.picId).isEmpty
        if (p.userId === user.id.get)
    } yield p
    db.run(query.result)
  }

  def getAll(user: User): Future[Seq[Picture]] = {
    val query = pictures.filter(_.userId === user.id)
    db.run(query.result)
  }

  def getBy(id: Int): Future[Option[Picture]] = {
    val query = pictures
      .filter(_.id === id)
      .result
      .headOption
    db.run(query)
  }

  def getBy(post: Post): Future[Seq[Picture]] = {
    val query = for {
      (pc, p) <- postContents.filter(_.postId.? === post.id) join pictures on ( _.picId === _.id )
    } yield p
    db.run(query.result)
  }

  def remove(picture: Picture): Future[Boolean] = {
    val query = pictures.filter(_.id.? === picture.id)
    db.run(query.delete).map {
      case num if (num > 0) => true
      case _ => false
    }
  }

  // def getIdByUniqueName(name: String): Future[Option[Int]] = {
  //   for {
  //     optPic <- getBy(name)
  //   } yield optPic flatMap (_.id)
  // }

}
