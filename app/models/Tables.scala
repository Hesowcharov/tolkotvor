
package models

import com.github.nscala_time.time.Imports._
import utils.ColumnTypeMappers._
import models.entities.{Tag => UserTag, _}
import slick.driver.PostgresDriver.api._

import javax.inject.Inject
/**
  * Created by hesowcharov on 26.01.17.
  */
object Tables {
  val pictureNotations = TableQuery[PictureNotations]
  val pictures = TableQuery[Pictures]
  val users = TableQuery[Users]
  val tags = TableQuery[Tags]
  val tagsPictures = TableQuery[TagsPictures]
  val textDatas = TableQuery[TextDatas]
  val textNotations = TableQuery[TextNotations]
  val posts = TableQuery[Posts]
  val plainTexts = TableQuery[PlainTexts]
  val postContents = TableQuery[PostContents]

  class Users @Inject()(tag: Tag) extends Table[User](tag, "users") {
    def id = column[Int]("user_id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def email = column[String]("email")
    def password = column[String]("password")
    def registered = column[DateTime]("registered")

    def * = (id.?, name, email.?, password, registered) <> (User.tupled, User.unapply)
  }

  class Pictures(tag: Tag) extends Table[Picture](tag, "pictures") {
    def id = column[Int]("pic_id", O.PrimaryKey, O.AutoInc)
    def userId = column[Int]("user_id")
    def width = column[Int]("width")
    def height = column[Int]("height")
    def format = column[String]("format", O.Length(10))
    def name = column[String]("name", O.Length(60))
    def description = column[String]("description")
    def uploaded = column[DateTime]("uploaded")
    def userFk = foreignKey("pictures_user_id_fkey", userId, users)(_.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.SetNull)

    def * = (id.?, userId, width, height, format.?, name, description, uploaded) <> (Picture.tupled, Picture.unapply)
  }

  class PictureNotations(tag: Tag) extends Table[PictureNotation](tag, "pic_notations") {
    import utils.ColumnTypeMappers.dateTimeMapper

    def id = column[Int]("pic_notation_id", O.PrimaryKey, O.AutoInc)
    def pictureId = column[Int]("pic_id")
    def userId = column[Int]("user_id")
    def xCoord = column[Int]("xcoord")
    def yCoord = column[Int]("ycoord")
    def comment = column[String]("comment")
    def created = column[DateTime]("created")

    def pictureFK = foreignKey("pic_notations_pic_id_fkey", pictureId, pictures)(
      _.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
    def userFK = foreignKey("pic_notations_user_id_fkey", userId, users)(_.id, onUpdate = ForeignKeyAction.Cascade, onDelete = ForeignKeyAction.SetNull)

    def * = (id.?, pictureId, userId, xCoord, yCoord, comment, created) <> (PictureNotation.tupled, PictureNotation.unapply)
  }

  class Tags(tag: Tag) extends Table[UserTag](tag, "tags") {
    def id = column[Int]("tag_id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name", O.Length(25))
    def initiatorId = column[Int]("initiator_id")

    def userFK = foreignKey("tags_initiator_id_fkey", initiatorId, users)(
      _.id, onDelete = ForeignKeyAction.SetNull)

    def * = (id.?, name, initiatorId) <> (UserTag.tupled, UserTag.unapply)
  }

  class TagsPictures(tag: Tag) extends Table[TagPicture](tag, "tags_pictures") {
    def tagId = column[Int]("tag_id")
    def picId = column[Int]("pic_id")

    def tagFK = foreignKey("tags_pictures_tag_id_fkey", tagId, tags) (
      _.id, onDelete = ForeignKeyAction.SetNull
    )
    def picFK = foreignKey("tags_pictures_pic_id_fkey", picId, pictures) (
      _.id, onDelete = ForeignKeyAction.Cascade
    )

    def * = (tagId, picId) <> (TagPicture.tupled, TagPicture.unapply)
  }

  class TextDatas(tag: Tag) extends Table[TextData](tag, "text_data") {
    def id = column[Int]("text_id", O.PrimaryKey, O.AutoInc)
    def userId = column[Int]("user_id")
    def title = column[String]("title")
    def text = column[Array[String]]("lines")
    def created = column[DateTime]("created")

    def userFK = foreignKey("text_data_user_id_fkey", userId, users) (
      _.id, onDelete = ForeignKeyAction.SetNull
    )

    def * = (id.?, userId, title.?, text, created) <> (TextData.tupled, TextData.unapply)
  }

  class TextNotations(tag: Tag) extends Table[TextNotation](tag, "text_notations") {
    def id = column[Int]("text_not_id", O.PrimaryKey, O.AutoInc)
    def textId = column[Int]("text_id")
    def userId = column[Int]("user_id")
    def line = column[Int]("line")
    def notation = column[String]("notation")
    def created = column[DateTime]("created")

    def textFK = foreignKey("text_notations_text_id_fkey", textId, textDatas)(
      _.id, onDelete = ForeignKeyAction.Cascade, onUpdate = ForeignKeyAction.Cascade
    )

    def userFK = foreignKey("text_notations_user_id_fkey", userId, users)(
      _.id, onDelete = ForeignKeyAction.SetNull, onUpdate = ForeignKeyAction.Cascade
    )

    def * = (id.?, textId, userId, line, notation, created) <> (TextNotation.tupled, TextNotation.unapply)
  }

  class PlainTexts(tag: Tag) extends Table[PlainText](tag, "plaintexts") {
    def id = column[Int]("plaintext_id", O.PrimaryKey, O.AutoInc)
    def postId = column[Int]("post_id")
    def text = column[String]("txt")
    def created = column[DateTime]("created")

    def postFK = foreignKey("plaintexts_post_id_fkey", postId, posts)(_.id, onUpdate = ForeignKeyAction.Cascade, onDelete = ForeignKeyAction.Cascade)

    def * = (id.?, postId, text, created) <> (PlainText.tupled, PlainText.unapply)
  }

  class Posts(tag: Tag) extends Table[Post](tag, "posts") {
    def id = column[Int]("post_id", O.PrimaryKey, O.AutoInc)
    def userId = column[Int]("user_id")
    def title = column[String]("title", O.Length(100))
    def created = column[DateTime]("created")
    def updated = column[DateTime]("updated")

    def userFK = foreignKey("posts_user_id_fkey", userId, users)(
      _.id, onDelete = ForeignKeyAction.SetNull
    )

    def * = (id.?, userId, title, created, updated) <> (Post.tupled, Post.unapply)
  }

  class PostContents(tag: Tag) extends Table[PostContent](tag, "post_contents") {
    val name = "post_contents"
    def postId = column[Int]("post_id")
    def picId = column[Int]("pic_id")
    def textId = column[Int]("text_id")
    def plainTextId = column[Int]("plaintext_id")
    def order = column[Int]("order_number")

    def postFK = foreignKey(s"${name}_post_id_fkey", postId, posts)(_.id,  onDelete = ForeignKeyAction.Cascade, onUpdate = ForeignKeyAction.Cascade)
    def picFK = foreignKey(s"${name}_pic_id_fkey", picId, pictures)(
      _.id, onDelete = ForeignKeyAction.Cascade, onUpdate = ForeignKeyAction.Cascade
    )
    def textFK = foreignKey(s"${name}_text_id_fkey", textId, textDatas)(
      _.id, onDelete = ForeignKeyAction.Cascade, onUpdate = ForeignKeyAction.Cascade
    )
    def plainTextFK = foreignKey(s"${name}_plaintext_id_fkey", plainTextId, plainTexts)(
      _.id, onDelete = ForeignKeyAction.Cascade, onUpdate = ForeignKeyAction.Cascade
    )

    def * = (postId, picId.?, textId.?, plainTextId.?, order.?) <> (PostContent.tupled, PostContent.unapply)
  }
}
