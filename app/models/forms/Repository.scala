package models.forms

import models.entities.PictureNotation
import play.api.data._
import play.api.data.Forms._


/**
  * Created by HesowcharovU on 13.10.2016.
  */
object Repository {
  val notationForm = Form(
    tuple(
      "x coordinate" -> number,
      "y coordinate" -> number,
      "comment" -> text
    )
  )
}
