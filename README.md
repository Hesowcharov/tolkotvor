# Tolkotvor #

This project is web blog where people can review some kind of art (picture, lyrics).

### How to install ###

* Install postgresql 9.6, sbt 0.13.13
* Create database 'tolkotvor_db' and user 'tolkotvor' with password
* Edit parameters "slick.dbs.default.db.PARAMETER" of config file conf/application.conf according to yours
* Use a http-server (for example, nginx) for proxying (application will be run on localhost:9000 by default) and providing static content (images). Example nginx config:

```
#!nginx
user  nginx;
worker_processes  1;

events {
    worker_connections  1024;
}

http {
    include       mime.types;
    default_type  application/octet-stream;

    sendfile        on;
    keepalive_timeout  65;

    server {
        listen       80;
        server_name  localhost;

        location / {
            proxy_pass	http://localhost:9000;
        }

	location /static/ {
	    alias   /home/user/tolkotvor-media/;
	}
    }
}
```
* Edit parameters "application.media.path" and "application.static.route" of config file conf/application.conf according to yours
* Run "sbt run" in terminal