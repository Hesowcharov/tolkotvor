# All schemas

# --- !Ups


CREATE TABLE users (
    user_id serial PRIMARY KEY,
    name varchar(30) UNIQUE NOT NULL,
    email varchar(60) NULL,
    password varchar(80) NOT NULL,
    registered timestamp NOT NULL
);

CREATE TABLE pictures (
    pic_id serial PRIMARY KEY,
    user_id integer NOT NULL REFERENCES users ON UPDATE CASCADE ON DELETE SET NULL,
    name varchar(60) NOT NULL,
    description text NULL,
    width integer NOT NULL,
    height integer NOT NULL,
    format varchar(10) NULL,
    uploaded timestamp NOT NULL
);

CREATE TABLE pic_notations (
    pic_notation_id serial PRIMARY KEY,
    pic_id integer REFERENCES pictures ON DELETE CASCADE ON UPDATE CASCADE,
    user_id integer NOT NULL REFERENCES users ON DELETE SET NULL ON UPDATE CASCADE,
    xcoord smallint NOT NULL,
    ycoord smallint NOT NULL,
    comment text NOT NULL,
    created timestamp NOT NULL
);

CREATE TABLE tags (
    tag_id serial PRIMARY KEY,
    name varchar(25) UNIQUE NOT NULL,
    initiator_id integer REFERENCES users ON DELETE SET NULL
);

CREATE TABLE tags_pictures (
    tag_id integer NOT NULL REFERENCES tags ON UPDATE CASCADE,
    pic_id integer NOT NULL REFERENCES pictures ON DELETE CASCADE
);

CREATE TABLE text_data (
    text_id serial PRIMARY KEY,
    user_id integer REFERENCES users ON DELETE SET NULL,
    title varchar(60) NULL,
    lines text NOT NULL,
    created timestamp NOT NULL
);

CREATE TABLE text_notations (
    text_not_id serial PRIMARY KEY,
    text_id integer REFERENCES text_data ON DELETE CASCADE ON UPDATE CASCADE,
    user_id integer REFERENCES users ON DELETE SET NULL ON UPDATE CASCADE,
    line integer NOT NULL,
    notation text NOT NULL,
    created timestamp NOT NULL
);

CREATE TABLE posts (
    post_id serial PRIMARY KEY,
    user_id integer REFERENCES users ON DELETE SET NULL,
    title varchar(100) NOT NULL,
    created timestamp NOT NULL,
    updated timestamp NOT NULL
);


CREATE TABLE plaintexts (
    plaintext_id serial PRIMARY KEY,
    post_id integer NOT NULL REFERENCES posts ON DELETE CASCADE ON UPDATE CASCADE,
    txt text NOT NULL,
    created timestamp NOT NULL
);

CREATE TABLE post_contents (
    post_id integer REFERENCES posts ON DELETE CASCADE ON UPDATE CASCADE,
    pic_id integer NULL REFERENCES pictures ON DELETE CASCADE ON UPDATE CASCADE,
    text_id integer NULL REFERENCES text_data ON DELETE CASCADE ON UPDATE CASCADE,
    plaintext_id integer NULL REFERENCES plaintexts ON DELETE CASCADE ON UPDATE CASCADE,
    order_number integer NULL
);

# --- !Downs

DROP TABLE post_contents;
DROP TABLE plaintexts;
DROP TABLE posts;
DROP TABLE text_notations;
DROP TABLE text_data;
DROP TABLE tags_pictures;
DROP TABLE tags;
DROP TABLE pic_notations;
DROP TABLE pictures;
DROP TABLE users;
