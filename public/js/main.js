function doLogout() {
  $.ajax({
    type: 'POST',
    headers: {
      'Csrf-Token': getCookie('Csrf-Token')
    },
    url: "/logout",
    complete: function(jqXHR, textStatus) {
      location.reload(true);
    }
  });
};

$(document).ready(function(){
  $('a[data-method]').on('click', function(event){
    event.preventDefault();
    $elem = $(this);
    var url = $elem.attr('href');
    var method = $elem.attr('data-method');
    $.ajax({
      type: method,
      headers: {
	'Csrf-Token': getCookie('Csrf-Token')
      },
      url: url,
      complete: function(jqXHR, textStatus) {
	location.reload(true);
      }
    });
  });
});

function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}

function redirectToSignIn() {
  window.location.replace('/signin');
}
