
$("#page-content").on("click", ".picture-review-canvas", function(ev) {
  var $optLocker = $('#locker');
  if ($optLocker.length &&
      $optLocker.attr('data-status') == 'OFF') {
    return;
  }
  var $picture = $(this);
  var $clicked = $(ev.target);
  var notationWasClicked = $clicked.hasClass("picture-review-notation");
  if (notationWasClicked) {
    var $notation = $clicked;
    var tumblerClass = "selected-notation";
    $notation.toggleClass(tumblerClass);
    var notationContent = $notation.children().first().clone();
    var notationReview = $picture.parent().find(".picture-review-show-notation");
    if($notation.hasClass(tumblerClass)) {
      notationContent.show();
      notationReview.html(notationContent).show();
    } else {
      notationReview.html("").hide();
    }
  } else {
    var relOffset = $clicked.offset();
    var picBorderOffset = parseInt($picture.css("border-left-width"), 10);
    var x = parseInt(ev.pageX - relOffset.left - picBorderOffset, 10);
    var y = parseInt(ev.pageY - relOffset.top - picBorderOffset, 10);
    var id = $picture.data("id");
    $("#picture-review-form").attr("action", "/pictures/" + id + "/notations");
    $("#picture-review-form input[name='x coordinate']").attr('value', x);
    $("#picture-review-form input[name='y coordinate']").attr('value', y);
    $('#notation-modal').modal('show');
  }
  
});

$('#page-content').on('click', '#submit-picture-notation', function(){
  var $form = $('#picture-review-form');
  $form.trigger('submit');
  $form.trigger('reset');
  $('#notation-modal').modal('hide');
});

function drawNotation(notation){
  var selector = `.picture-review[data-id='${notation.pictureId}'] .picture-review-canvas`;
  var pictureCanvas = $(selector);
  var notationDiv = $('<div/>', {
    'class':"picture-review-notation",
    'style': `top: ${notation.y-10}px; left: ${notation.x-10}px;`,
    'html': notation.fragment
  }).show().appendTo(pictureCanvas);
};

$('#page-content').on('submit', '#picture-review-form', function(e){
  $.ajax({
    type: 'POST',
    url: $(this).attr("action"),
    data: new FormData(this),
    processData: false,
    contentType: false,
    success: function(data) {
      drawNotation(data);
    },
    error: function(xhr, textStatus, errorThrown){
      console.log('request failed');
      console.log(xhr);
      console.log(textStatus);
      console.log(errorThrown);
      if (xhr.status === 403) {
	redirectToSignIn();
      }
    }
  });
  e.preventDefault();
});
