$("#picture-form").submit(function(e) {
  $.ajax({
    type: 'POST',
    url: '/pictures',
    headers: {
      'Csrf-Token': getCookie('Csrf-Token')
    },
    data: new FormData(this),
    processData: false,
    contentType: false,
    success: function(data) {
      window.location = "/pictures/" + data.id
    },
    error: function(xhr, textStatus, errorThrown){
      console.log('request failed');
      console.log(xhr);
      console.log(textStatus);
      console.log(errorThrown);
      if (xhr.status === 403) {
	redirectToSignIn();
      }
    }
  });
  e.preventDefault();
});
