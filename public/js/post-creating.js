var plainTextForm = $("#plaintext-form");
var pictureForm = $("#picture-form");
var textForm = $("#text-form");
var forms =
  [
    plainTextForm,
    pictureForm,
    textForm
  ];
var postId = $(".post").data("id");
var fragmentController = $("#fragment-controller");
var modeSelector = $("#mode-select");
var fragmentSelector = $("#fragment-select");
var anchorFragment = $("#anchor-post-fragment");
var modes = {
  BEFORE: 0,
  AFTER: 1,
  REMOVE: 2
};
var locker = $("#locker");
var lockStatuses = {
  ON: 0,
  OFF: 1
};

$(document).ready(function() {
  var styledClasses = "col-md-8 col-md-offset-2 well";
  pictureForm.removeClass(styledClasses);
  textForm.removeClass(styledClasses);
  resetFragmentController();
});

function getMode() {
  return parseInt(modeSelector.val(), 10);  
};

function getSelectedElement() {
  var result = $(".post-fragment.selected-fragment")
  return (result.length > 0) ? result : $(".post-fragment");
};

function selectElement(element) {
  var selectionClasses = "selected-fragment bs-callout bs-callout-info";
  $(".post-fragment").removeClass(selectionClasses);
  if (element === null) { return; }
  element.toggleClass(selectionClasses);
};

function resetForms() {
  forms.forEach(f => f.trigger("reset"));
};

function changeMode(element, mode) {
  if(mode === modes.BEFORE) {
    element.prepend(fragmentController);
    /* fragmentController.insertBefore(element);*/
  } else if (mode === modes.AFTER) {
    element.append(fragmentController);
    /* fragmentController.insertAfter(element);*/
  } else if (mode === modes.REMOVE) {
    var removingIsConfirmed = confirm("Are you sure ?");
    if (!removingIsConfirmed) {
      changeMode(element, modes.AFTER);
      return;
    }
    var id = element.data("id");
    var type = element.data("type");
    $.ajax({
      url: "/" + type + "/" + id,
      headers: {
	'Csrf-Token': $('input[name="csrfToken"]').first().val()
      },
      type: "DELETE",
      success: function(data, status, xhr) {
	window.location.href = "/posts/" + postId + '/edit';
      }
    });
  }
};

function showForm(number) {
  forms.forEach(f => f.hide());
  forms[number].show();
};

modeSelector.change(function() {
  var mode = getMode();
  var element = getSelectedElement();
  changeMode(element, mode);
});

fragmentSelector.change(function() {
  var selected = parseInt(fragmentSelector.val(), 10);
  showForm(selected);
});

$("#page-content").on("click", ".post-fragment", function(event) {
  var status = getLockStatus();
  if (status == lockStatuses.OFF) {
    /* event.stopImmediatePropagation();*/
    /* event.preventDefault();*/
    var jqElement = $(this);
    if (jqElement.hasClass('selected-fragment')) {
      return;
    }
    var mode = getMode();
    selectElement(jqElement);
    changeMode(jqElement, mode);
  }
});

function insertNewFragment(fragment, id, type, mode, element) {
  if(mode === modes.BEFORE) {
    element.before(fragment);
  } else if (mode === modes.AFTER) {
    element.after(fragment);
  }
  var newElement = $('.post-fragment.' + type + "[data-id=\"" + id + "\"]");
  selectElement(newElement);
  changeMode(newElement, modes.AFTER);
};

function bindNewFragment(type, id, nextAction) {
  var bindUrl = '/posts/' + postId + '/' + type + '/';
  $.ajax({
    type: 'POST',
    headers: {
      'Csrf-Token': $('input[name="csrfToken"]').first().val()
    },
    url: bindUrl + id,
    success: function() {
      if(nextAction) nextAction();
    }
  });
};


$("#picture-review-form").submit(function(e){
  $.ajax({
    type: 'POST',
    url: $(this).attr("action"),
    headers: {
      'Csrf-Token': $('input[name="csrfToken"]').first().val()
    },
    data: new FormData(this),
    processData: false,
    contentType: false,
    success: function() {
      $("#dialog-wrapper").hide();
    },
    error: function(xhr, textStatus, errorThrown){
      console.log('request failed');
      console.log(xhr);
      console.log(textStatus);
      console.log(errorThrown);
      if (xhr.status === 403) {
	redirectToSignIn();
      }
    }
  });
  e.preventDefault();
});

textForm.submit(function(e) {
  var mode = getMode();
  var selectedElement = getSelectedElement();
  var type = "texts";
  $.ajax({
    type: 'POST',
    url: '/' + type,
    data: new FormData(this),
    headers: {
      'Csrf-Token': $('input[name="csrfToken"]').first().val()
    },
    processData: false,
    contentType: false,
    success: function(data){
      var id = data.id;
      var thenInsert = function() {
	$.get("/text-fragments/" + id, function(fragment) {
	  insertNewFragment(fragment, id, 'text-review', mode, selectedElement);
	});
      };
      bindNewFragment(type, id, thenInsert);
    },
    error: function(xhr, textStatus, errorThrown){
      console.log('request failed');
      console.log(xhr);
      console.log(textStatus);
      console.log(errorThrown);
      if (xhr.status === 403) {
	redirectToSignIn();
      }
    }
  });
  resetForms();
  e.preventDefault();
});

pictureForm.submit(function(e) {
  replaceControllerOnSpinner();
  var mode = getMode();
  var selectedElement = getSelectedElement();
  var type = "pictures";
  $.ajax({
    type: 'POST',
    url: '/' + type,
    data: new FormData(this),
    processData: false,
    headers: {
      'Csrf-Token': $('input[name="csrfToken"]').first().val()
    },
    contentType: false,
    success: function(data) {
      var id = data.id;
      var thenInsert = function() {
	$.get("/picture-fragments/" + id, function(fragment) {
	  insertNewFragment(fragment, id, 'picture-review', mode, selectedElement);
	});
      };
      bindNewFragment(type, id, thenInsert);
    },
    error: function(xhr, textStatus, errorThrown){
      console.log('request failed');
      console.log(xhr);
      console.log(textStatus);
      console.log(errorThrown);
      if (xhr.status === 403) {
	redirectToSignIn();
      }
    },
    complete: replaceSpinnerOnController()
  });
  resetForms();
  e.preventDefault();
});

plainTextForm.submit(function(e) {
  replaceControllerOnSpinner();
  var mode = getMode();
  var selectedElement = getSelectedElement();
  var type = "ptexts";
  $.ajax({
    type: 'POST',
    url: '/posts/' + postId + '/' + type,
    headers: {
      'Csrf-Token': $('input[name="csrfToken"]').first().val()
    },
    data: new FormData(this),
    processData: false,
    contentType: false,
    success: function(data){
      var id = data.id;
      $.get("/" + type + "/" + id, function(fragment) {
	insertNewFragment(fragment, data.id, 'plain-text', mode, selectedElement);
      });
    },
    error: function(xhr, textStatus, errorThrown){
      console.log('request failed');
      console.log(xhr);
      console.log(textStatus);
      console.log(errorThrown);
      if (xhr.status === 403) {
	redirectToSignIn();
      }
    },
    complete: replaceSpinnerOnController()
  });
  resetForms();
  e.preventDefault();
});

function updateAll() {
  var postTitle = $(".post #post-title").val();
  var postObj = {
    'postId': postId,
    'postTitle': postTitle,
    'fragments': []
  };
  $(".post-fragment:not(#anchor-post-fragment)").each( (idx, elem) => {
    var jqElement = $(elem);
    var fragmentId = jqElement.data("id");
    var type = jqElement.data("type");

    postObj.fragments.push({
      'type': type,
      'fragmentId': fragmentId,
      'order': idx + 1
    });
  });
  $.ajax({
    type: 'POST',
    url: "/posts/" + postId,
    headers: {
      'Csrf-Token': $('input[name="csrfToken"]').first().val()
    },
    data: JSON.stringify(postObj),
    contentType: 'application/json',
    dataType: 'json',
    success: function(data) { console.log("get back from server"); }
  });
  
};

function moveFragment(toUp) {
  var anchorId = anchorFragment.attr("id");
  var selectedElement = getSelectedElement();
  if (selectedElement.attr('id') == anchorId) {
    return;
  }
  var selector = ".post-fragment:not(#" + anchorId + ")";
  var siblings = toUp ? selectedElement.prevAll(selector)
	      : selectedElement.nextAll(selector);
  if (siblings.length == 0) {
    return;
  }
  var sibling = siblings[0];
  if (toUp) {
    selectedElement.insertBefore(sibling);
  } else {
    selectedElement.insertAfter(sibling);
  }
  var mode = getMode();
  changeMode(selectedElement, mode);
}

function tumbleLocker(status) {
  if (status == lockStatuses.ON) {
    locker.attr("data-status", "OFF");
    locker.removeClass('btn-basic');
    locker.addClass('btn-default');
    locker.html("Unlocked post");
    initFragmentController();
  } else if (status == lockStatuses.OFF) {
    locker.attr("data-status", "ON");
    locker.removeClass('btn-default');
    locker.addClass('btn-basic');
    locker.html("Locked post");
    resetFragmentController();
  }
};

function initFragmentController() {
  var initForm = 0;
  var initMode = modes.AFTER;
  var initElement = $(".post-fragment").last();
  showForm(initForm);
  if(initElement.is('#anchor-post-fragment')) {
    initElement.show();
  }
  selectElement(initElement);
  changeMode(initElement, initMode);
  fragmentController.show();
};

function resetFragmentController() {
  selectElement(null);
  fragmentController.hide();
}

function replaceControllerOnSpinner(){
  $('.controller').hide();
  $('.spinner').show();
}

function replaceSpinnerOnController(){
  $('.spinner').hide();
  $('.controller').show();
}

locker.click(function() {
  var status = getLockStatus();
  tumbleLocker(status);
});

function getLockStatus() {
  return locker.attr("data-status") == "ON" ? lockStatuses.ON : lockStatuses.OFF;
};
