
function getSelectedParagraph() {
  if (window.getSelection) {
    selection = window.getSelection();
  }
  /*case of old IE browsers*/
  else if (document.selection) {
    selection = document.selection.createRange();
  }
  var parEl = selection.focusNode.parentElement;
  return parEl;
}

$("#page-content").on("click", ".text-review p[data-index-number]", function(ev, update, getUrl) {
  var $optLocker = $('#locker');
  if ($optLocker.length &&
      $optLocker.attr('data-status') == 'OFF') {
    return;
  }
  var notationsUri = getUrl;
  if (!notationsUri) {
    var par = getSelectedParagraph(),
	parData = par.dataset,
	line = parData.indexNumber,
	divData = par.parentElement.parentElement.dataset,
	textId = divData.id;
    notationsUri = "/texts/" + textId + "/lines/" + line + "/notations";
  }
  var hiddenDiv = $("#text-review-line");
  hiddenDiv.hide();
  var hiddenForm = $("#text-review-line-form");
  var prevNotationsUri = hiddenForm.attr("action");
  if (notationsUri === prevNotationsUri && !update) {
    hiddenForm.attr("action", "?");
    return;
  }
  hiddenForm.attr("action", notationsUri);
  hiddenForm.find("input[name='text-id']").attr('value', textId);
  hiddenForm.find("input[name='line-number']").attr('value', line);
  
  $.get(notationsUri, function(data) {
    console.log(data);
    var notationsHtml = "";
    if (!data.length) {
      notationsHtml = "Your comment will be first!"
    } else {
      data.forEach( obj => {
	var comment = obj.notation.text;
	var userInfo = '';
	if (obj.userInfo) {
	  userInfo += '<cite>' + '<a href="/users/' + obj.userInfo.id +  '">' +
		    obj.userInfo.name + '</a></cite>';
	} else {
	  userInfo += '<cite>unknown user</cite>';
	}
	
	var meta =  obj.notation.created + ' by ' + userInfo;
	var escapedNotation = safe_tags_replace(comment);
	notationsHtml += "<p>" + escapedNotation + "</p>";
	notationsHtml += "<footer>" + meta + "</footer>";
      });
    }
    var notationDiv = $("#text-review-line-notations");
    notationDiv.html(notationsHtml);
    hiddenDiv.insertAfter(par);
    hiddenDiv.show();
  });
  
});

$("#page-content").on("submit", "#text-review-line-form", function(event) {
  event.preventDefault();
  var $form = $(this);
  $.ajax({
    type: 'POST',
    processData: false,
    contentType: false,
    headers: {
      'Csrf-Token': $('input[name="csrfToken"]').first().val()
    },
    url: $form.attr('action'),
    data: new FormData(this),
    success: function(json) {
      $form.trigger('reset');
      var lineSelector = '#page-content ';
      lineSelector += '.text-review[data-id=' + json.text_id + '] ';
      lineSelector += 'p[data-index-number=' + json.line  + ']';
      var resourceUrl = '/texts/' + json.text_id + '/lines/' + json.line + '/notations';
      /* var $paragraph = $(lineSelector);
       * var selection = window.getSelection();
       * var range = document.createRange();
       * range.selectNodeContents($paragraph[0]);
       * selection.removeAllRanges();
       * selection.addRange(range);*/
      $(lineSelector).trigger('click', ['update', resourceUrl]);
      /* selection.removeAllRanges();*/
    },
    error: function(xhr) {
      if (xhr.status === 403) {
	redirectToSignIn();
      }
    }
  })
});

  var tagsToReplace = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#39;',
  '/': '&#x2F;',
  '`': '&#x60;',
  '=': '&#x3D;'
};

function replaceTag(tag) {
  return tagsToReplace[tag] || tag;
}

function safe_tags_replace(str) {
  return str.replace(/[&<>"'`=\/]/g, replaceTag);
}
